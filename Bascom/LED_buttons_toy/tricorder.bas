$regfile = "attiny2313.dat"
$crystal = 1000000
$hwstack = 36
$swstack = 20
$framesize = 36

Config Base = 0                                             ' index starts at 0

Ddra = &B11111100                                           ' mask for pin input/output, 1 - output, 0 - input
Ddrb = &B10100000                                           ' temporary disable input PB6, Ddrb = &B10100000 - floating pin internally bounce
Ddrd = &B10111000


Porta = &B00000011                                          ' internal pull-up resistors, 1 - on, 0 - off
Portb = &B01011111
Portd = &B00000111                                          ' PD6 pull-up only 2,5 V, not enough, external pull-up


Data_clock Alias Portd.3
Latch_clock Alias Portd.4
Data_out Alias Portd.5

Set Data_clock
Reset Latch_clock
Reset Data_out

Dim A As Byte
Dim B As Byte
Dim Ic_led As Word
Dim Ic_display As Byte
Dim Speed As Byte
Dim Sw8time As Byte
Dim Buttons As Byte
Dim Buttons_new As Byte
Dim Bcdenc As Byte
Dim Bcdenc_new As Byte
Dim Sw3semaphore As Byte

Ic_led = &B1010001001010100
'          |   |   |   |
Sw3semaphore = 1


Do

Bcdenc_new.0 = Pinb.0
Bcdenc_new.1 = Pinb.1
Bcdenc_new.2 = Pinb.2
Bcdenc_new.3 = Pinb.3

Buttons_new.0 = Pina.0                                      'SW 02
Buttons_new.1 = Pina.1                                      'SW 03
Buttons_new.2 = Pinb.4                                      'SW 07
Buttons_new.3 = Pinb.6                                      'SW 08
Buttons_new.4 = Pind.0                                      'SW 05
Buttons_new.5 = Pind.1                                      'SW 04
Buttons_new.6 = Pind.2                                      'SW 01
Buttons_new.7 = Pind.6                                      'SW 06

If Speed = 0 Then
   Select Case Bcdenc_new.0
      Case 0 : Speed = 70
      Case 1 : Speed = 40
   End Select
End If

Decr Speed

If Buttons_new.0 = 0 And Speed = 0 Then
   Toggle Ic_led.13
   Toggle Ic_led.12
End If

If Buttons_new.1 = 0 And Speed = 0 Then Incr Sw3semaphore
If Sw3semaphore > 4 Then Sw3semaphore = 1

If Buttons_new.2 < Buttons.2 Then Toggle Ic_led.1

If Buttons_new.3 < Buttons.3 Then
   Toggle Ic_led.0
   Set Ic_led.11
   Sw8time = 3
End If

If Buttons_new.4 < Buttons.4 Then
   Toggle Ic_led.4
   Toggle Ic_led.3
End If

If Buttons_new.5 = 0 And Speed = 0 Then
   Toggle Ic_led.6
   Toggle Ic_led.7
End If

If Buttons_new.6 < Buttons.6 Then
   Toggle Ic_led.14
   Toggle Ic_led.15
End If

If Buttons_new.7 < Buttons.7 Then Toggle Ic_led.2

If Buttons_new.6 = 0 And Buttons_new.4 = 0 Then Set Ic_led.5 Else Reset Ic_led.5


If Sw3semaphore = 1 Then
   Set Ic_led.10                                            'red
   Reset Ic_led.9                                           'yellow
   Reset Ic_led.8                                           'green
End If

If Sw3semaphore = 2 Then
   Set Ic_led.10                                            'red
   Set Ic_led.9                                             'yellow
   Reset Ic_led.8                                           'green
End If

If Sw3semaphore = 3 Then
   Reset Ic_led.10                                          'red
   Reset Ic_led.9                                           'yellow
   Set Ic_led.8                                             'green
End If

If Sw3semaphore = 4 Then
   Reset Ic_led.10                                          'red
   Set Ic_led.9                                             'yellow
   Reset Ic_led.8                                           'green
End If




If Ic_led.11 = 1 And Speed = 0 Then Decr Sw8time
If Sw8time = 0 Then Reset Ic_led.11

If Speed = 0 Then Incr Ic_display

Buttons = Buttons_new
Bcdenc = Bcdenc_new

Shiftout Data_out , Data_clock , Ic_display , 3
Shiftout Data_out , Data_clock , Ic_led , 3

Set Latch_clock
Waitms 10
Reset Latch_clock

Waitms 10

Loop

End


' program :
' 0-9
'           0 0 0 0
'           0 0 0 1
'           0 0 1 0
'           0 0 1 1
'           0 1 0 0
'           0 1 0 1
'           0 1 1 0
'           0 1 1 1
'           1 0 0 0
'           1 0 0 1
'
'           x y z q
'
'   x - 0 normal, 1 full random at every push
'   y - 0 normal, 1 button-led randomizer
'   z - 0 on/off, 1 toggle
'   q - 0 slow, 1 fast times