EESchema Schematic File Version 4
LIBS:MCP23016_break_out-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23016_break_out-rescue:MCP23016 U1
U 1 1 590399A9
P 5550 3550
F 0 "U1" H 5450 4575 50  0000 R CNN
F 1 "MCP23016" H 5450 4500 50  0000 R CNN
F 2 "cubetto_clone:MCP23016_SOIC-28" H 5700 2600 50  0001 L CNN
F 3 "" H 5800 4550 50  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X14 J2
U 1 1 59039D73
P 7100 3400
F 0 "J2" H 7100 4150 50  0000 C CNN
F 1 "CONN_01X14" V 7200 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 7100 3400 50  0001 C CNN
F 3 "" H 7100 3400 50  0001 C CNN
	1    7100 3400
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X14 J1
U 1 1 59039D96
P 3750 3450
F 0 "J1" H 3750 4200 50  0000 C CNN
F 1 "CONN_01X14" V 3850 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 3750 3450 50  0001 C CNN
F 3 "" H 3750 3450 50  0001 C CNN
	1    3750 3450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5450 4550 4450 4550
Wire Wire Line
	4450 4550 4450 2800
Wire Wire Line
	4450 2800 3950 2800
Wire Wire Line
	5050 2750 4600 2750
Wire Wire Line
	4600 2750 4600 2900
Wire Wire Line
	4600 2900 3950 2900
Wire Wire Line
	5050 2850 4650 2850
Wire Wire Line
	4650 2850 4650 3000
Wire Wire Line
	4650 3000 3950 3000
Wire Wire Line
	5050 2950 4700 2950
Wire Wire Line
	4700 2950 4700 3100
Wire Wire Line
	4700 3100 3950 3100
Wire Wire Line
	5050 3050 4750 3050
Wire Wire Line
	4750 3050 4750 3200
Wire Wire Line
	4750 3200 3950 3200
Wire Wire Line
	6050 3250 6250 3250
Wire Wire Line
	6250 3250 6250 2400
Wire Wire Line
	6250 2400 4350 2400
Wire Wire Line
	4350 2400 4350 3300
Wire Wire Line
	4350 3300 3950 3300
Wire Wire Line
	5050 3150 4800 3150
Wire Wire Line
	4800 3150 4800 3400
Wire Wire Line
	4800 3400 3950 3400
Wire Wire Line
	5550 4550 5550 4600
Wire Wire Line
	5550 4600 4400 4600
Wire Wire Line
	4400 4600 4400 3500
Wire Wire Line
	4400 3500 3950 3500
Wire Wire Line
	6050 2750 6050 2300
Wire Wire Line
	6050 2300 4300 2300
Wire Wire Line
	4300 2300 4300 3600
Wire Wire Line
	4300 3600 3950 3600
Wire Wire Line
	6050 2850 6150 2850
Wire Wire Line
	6150 2850 6150 2200
Wire Wire Line
	6150 2200 4250 2200
Wire Wire Line
	4250 2200 4250 3700
Wire Wire Line
	4250 3700 3950 3700
Wire Wire Line
	5050 3250 4850 3250
Wire Wire Line
	4850 3250 4850 3800
Wire Wire Line
	4850 3800 3950 3800
Wire Wire Line
	5050 3350 4900 3350
Wire Wire Line
	4900 3350 4900 3900
Wire Wire Line
	4900 3900 3950 3900
Wire Wire Line
	5050 3450 4950 3450
Wire Wire Line
	4950 3450 4950 4000
Wire Wire Line
	4950 4000 3950 4000
Wire Wire Line
	6050 3850 6200 3850
Wire Wire Line
	6200 3850 6200 4650
Wire Wire Line
	6200 4650 4350 4650
Wire Wire Line
	4350 4650 4350 4100
Wire Wire Line
	4350 4100 3950 4100
Wire Wire Line
	6050 3950 6150 3950
Wire Wire Line
	6150 3950 6150 4050
Wire Wire Line
	6150 4050 6900 4050
Wire Wire Line
	6050 4150 6350 4150
Wire Wire Line
	6350 4150 6350 3950
Wire Wire Line
	6350 3950 6900 3950
Wire Wire Line
	6050 4250 6500 4250
Wire Wire Line
	6500 4250 6500 3850
Wire Wire Line
	6500 3850 6900 3850
Wire Wire Line
	6050 4350 6600 4350
Wire Wire Line
	6600 4350 6600 3750
Wire Wire Line
	6600 3750 6900 3750
Wire Wire Line
	5650 4550 6700 4550
Wire Wire Line
	6700 4550 6700 3650
Wire Wire Line
	6700 3650 6900 3650
Wire Wire Line
	5550 2550 6350 2550
Wire Wire Line
	6350 2550 6350 3550
Wire Wire Line
	6350 3550 6900 3550
Wire Wire Line
	5050 3650 4500 3650
Wire Wire Line
	4500 3650 4500 4750
Wire Wire Line
	4500 4750 6750 4750
Wire Wire Line
	6750 4750 6750 3450
Wire Wire Line
	6750 3450 6900 3450
Wire Wire Line
	5050 3750 5000 3750
Wire Wire Line
	5000 3750 5000 4850
Wire Wire Line
	5000 4850 6800 4850
Wire Wire Line
	6800 4850 6800 3350
Wire Wire Line
	6800 3350 6900 3350
Wire Wire Line
	5050 3850 4600 3850
Wire Wire Line
	4600 3850 4600 4900
Wire Wire Line
	4600 4900 6850 4900
Wire Wire Line
	6850 4900 6850 3250
Wire Wire Line
	6850 3250 6900 3250
Wire Wire Line
	5050 3950 4650 3950
Wire Wire Line
	4650 3950 4650 4950
Wire Wire Line
	4650 4950 6650 4950
Wire Wire Line
	6650 4950 6650 3150
Wire Wire Line
	6650 3150 6900 3150
Wire Wire Line
	5050 4050 4700 4050
Wire Wire Line
	4700 4050 4700 5000
Wire Wire Line
	4700 5000 6550 5000
Wire Wire Line
	6550 5000 6550 3050
Wire Wire Line
	6550 3050 6900 3050
Wire Wire Line
	5050 4150 4750 4150
Wire Wire Line
	4750 4150 4750 5050
Wire Wire Line
	4750 5050 6450 5050
Wire Wire Line
	6450 5050 6450 2950
Wire Wire Line
	6450 2950 6900 2950
Wire Wire Line
	5050 4250 4800 4250
Wire Wire Line
	4800 4250 4800 5100
Wire Wire Line
	4800 5100 6300 5100
Wire Wire Line
	6300 5100 6300 2850
Wire Wire Line
	6300 2850 6900 2850
Wire Wire Line
	5050 4350 5050 5150
Wire Wire Line
	5050 5150 6400 5150
Wire Wire Line
	6400 5150 6400 2750
Wire Wire Line
	6400 2750 6900 2750
$EndSCHEMATC
