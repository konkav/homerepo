EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP8266
LIBS:hn58x2432
LIBS:logiclevelshifter
LIBS:witty
LIBS:ws2812
LIBS:cubetto_clone_controller-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23016 U1
U 1 1 5900B936
P 5800 2500
F 0 "U1" H 5700 3525 50  0000 R CNN
F 1 "MCP23016" H 5700 3450 50  0000 R CNN
F 2 "cubetto_clone:MCP23016_SOIC-28" H 5950 1550 50  0001 L CNN
F 3 "" H 6050 3500 50  0001 C CNN
	1    5800 2500
	1    0    0    -1  
$EndComp
$Comp
L MCP23016 U2
U 1 1 5900B99D
P 5800 5600
F 0 "U2" H 5700 6625 50  0000 R CNN
F 1 "MCP23016" H 5700 6550 50  0000 R CNN
F 2 "cubetto_clone:MCP23016_SOIC-28" H 5950 4650 50  0001 L CNN
F 3 "" H 6050 6600 50  0001 C CNN
	1    5800 5600
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR12
U 1 1 5900BC4E
P 9550 4100
F 0 "#PWR12" H 9550 3850 50  0001 C CNN
F 1 "Earth" H 9550 3950 50  0001 C CNN
F 2 "" H 9550 4100 50  0001 C CNN
F 3 "" H 9550 4100 50  0001 C CNN
	1    9550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3850 9550 4100
$Comp
L TEST_1P J6
U 1 1 5900BE31
P 8600 2600
F 0 "J6" H 8600 2870 50  0000 C CNN
F 1 "LDR_1" H 8600 2800 50  0000 C CNN
F 2 "Connectors:1pin" H 8800 2600 50  0001 C CNN
F 3 "" H 8800 2600 50  0001 C CNN
	1    8600 2600
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 5900C81F
P 6450 4550
F 0 "R4" V 6530 4550 50  0000 C CNN
F 1 "3.9k" V 6450 4550 50  0000 C CNN
F 2 "" V 6380 4550 50  0001 C CNN
F 3 "" H 6450 4550 50  0001 C CNN
	1    6450 4550
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5900C86E
P 6450 1450
F 0 "R3" V 6530 1450 50  0000 C CNN
F 1 "3.9k" V 6450 1450 50  0000 C CNN
F 2 "" V 6380 1450 50  0001 C CNN
F 3 "" H 6450 1450 50  0001 C CNN
	1    6450 1450
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5900C962
P 6600 1800
F 0 "C3" H 6625 1900 50  0000 L CNN
F 1 "33p" H 6625 1700 50  0000 L CNN
F 2 "" H 6638 1650 50  0001 C CNN
F 3 "" H 6600 1800 50  0001 C CNN
	1    6600 1800
	0    1    1    0   
$EndComp
$Comp
L C C4
U 1 1 5900C997
P 6600 4900
F 0 "C4" H 6625 5000 50  0000 L CNN
F 1 "33p" H 6625 4800 50  0000 L CNN
F 2 "" H 6638 4750 50  0001 C CNN
F 3 "" H 6600 4900 50  0001 C CNN
	1    6600 4900
	0    1    1    0   
$EndComp
$Comp
L +3V3 #PWR4
U 1 1 5900CDD2
P 6200 1200
F 0 "#PWR4" H 6200 1050 50  0001 C CNN
F 1 "+3V3" H 6200 1340 50  0000 C CNN
F 2 "" H 6200 1200 50  0001 C CNN
F 3 "" H 6200 1200 50  0001 C CNN
	1    6200 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1300 6200 1300
Wire Wire Line
	6200 1300 6350 1300
Wire Wire Line
	6350 1300 6450 1300
Wire Wire Line
	6200 1300 6200 1200
Wire Wire Line
	6300 1700 6450 1700
Wire Wire Line
	6450 1600 6450 1700
Wire Wire Line
	6450 1700 6450 1800
Connection ~ 6450 1700
Wire Wire Line
	5800 1500 5800 1300
Connection ~ 6200 1300
$Comp
L C C1
U 1 1 5900CE56
P 5800 1150
F 0 "C1" H 5825 1250 50  0000 L CNN
F 1 "100n" H 5825 1050 50  0000 L CNN
F 2 "" H 5838 1000 50  0001 C CNN
F 3 "" H 5800 1150 50  0001 C CNN
	1    5800 1150
	-1   0    0    1   
$EndComp
Connection ~ 5800 1300
$Comp
L Earth #PWR7
U 1 1 5900CF41
P 6600 950
F 0 "#PWR7" H 6600 700 50  0001 C CNN
F 1 "Earth" H 6600 800 50  0001 C CNN
F 2 "" H 6600 950 50  0001 C CNN
F 3 "" H 6600 950 50  0001 C CNN
	1    6600 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 950  5800 1000
Wire Wire Line
	6750 950  6750 1800
Wire Wire Line
	5800 950  6600 950 
Wire Wire Line
	6600 950  6750 950 
Connection ~ 6600 950 
$Comp
L C C2
U 1 1 5900D378
P 5800 4150
F 0 "C2" H 5825 4250 50  0000 L CNN
F 1 "100n" H 5825 4050 50  0000 L CNN
F 2 "" H 5838 4000 50  0001 C CNN
F 3 "" H 5800 4150 50  0001 C CNN
	1    5800 4150
	-1   0    0    1   
$EndComp
$Comp
L +3V3 #PWR5
U 1 1 5900DCC6
P 6200 4200
F 0 "#PWR5" H 6200 4050 50  0001 C CNN
F 1 "+3V3" H 6200 4340 50  0000 C CNN
F 2 "" H 6200 4200 50  0001 C CNN
F 3 "" H 6200 4200 50  0001 C CNN
	1    6200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4700 6450 4800
Wire Wire Line
	6450 4800 6450 4900
Wire Wire Line
	6300 4800 6450 4800
Connection ~ 6450 4800
Wire Wire Line
	5800 4400 6200 4400
Wire Wire Line
	6200 4400 6350 4400
Wire Wire Line
	6350 4400 6450 4400
Wire Wire Line
	5800 4300 5800 4400
Wire Wire Line
	5800 4400 5800 4600
Connection ~ 5800 4400
Wire Wire Line
	6200 4200 6200 4400
Connection ~ 6200 4400
$Comp
L Earth #PWR6
U 1 1 5900DDB2
P 6500 4000
F 0 "#PWR6" H 6500 3750 50  0001 C CNN
F 1 "Earth" H 6500 3850 50  0001 C CNN
F 2 "" H 6500 4000 50  0001 C CNN
F 3 "" H 6500 4000 50  0001 C CNN
	1    6500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4000 6750 4900
Wire Wire Line
	5800 4000 6500 4000
Wire Wire Line
	6500 4000 6750 4000
Connection ~ 6500 4000
Wire Wire Line
	5700 3500 5700 3600
Wire Wire Line
	5700 3600 5800 3600
Wire Wire Line
	5800 3600 5900 3600
Wire Wire Line
	5900 3600 6000 3600
Wire Wire Line
	5800 3500 5800 3600
Wire Wire Line
	5800 3600 5800 4000
Connection ~ 5800 3600
Wire Wire Line
	5900 3600 5900 3500
Connection ~ 5800 4000
$Comp
L Earth #PWR3
U 1 1 5900DF52
P 5800 6850
F 0 "#PWR3" H 5800 6600 50  0001 C CNN
F 1 "Earth" H 5800 6700 50  0001 C CNN
F 2 "" H 5800 6850 50  0001 C CNN
F 3 "" H 5800 6850 50  0001 C CNN
	1    5800 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 6600 5800 6700
Wire Wire Line
	5800 6700 5800 6850
Wire Wire Line
	5700 6600 5700 6700
Wire Wire Line
	5700 6700 5800 6700
Wire Wire Line
	5800 6700 5900 6700
Wire Wire Line
	5900 6700 6350 6700
Connection ~ 5800 6700
Wire Wire Line
	5900 6700 5900 6600
Wire Wire Line
	6200 3550 7050 3550
Wire Wire Line
	7050 3550 7950 3550
Wire Wire Line
	7050 2800 7050 3550
Wire Wire Line
	7050 3550 7050 5900
Wire Wire Line
	7050 2800 6300 2800
Wire Wire Line
	7050 5900 6300 5900
Connection ~ 7050 3550
Wire Wire Line
	6300 3650 6950 3650
Wire Wire Line
	6950 3650 7950 3650
Wire Wire Line
	6950 2900 6950 3650
Wire Wire Line
	6950 3650 6950 6000
Wire Wire Line
	6950 2900 6300 2900
Wire Wire Line
	6950 6000 6300 6000
Connection ~ 6950 3650
Text GLabel 7250 3550 1    60   Input ~ 0
SCL
Text GLabel 7250 3650 3    60   Input ~ 0
SDA
$Comp
L CONN_02X07 J3
U 1 1 590100B8
P 4400 2000
F 0 "J3" H 4400 2400 50  0000 C CNN
F 1 "Main_1" V 4400 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07_Pitch2.54mm" H 4400 800 50  0001 C CNN
F 3 "" H 4400 800 50  0001 C CNN
	1    4400 2000
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 J1
U 1 1 59010148
P 3900 2700
F 0 "J1" H 3900 3100 50  0000 C CNN
F 1 "Main_2" V 3900 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07_Pitch2.54mm" H 3900 1500 50  0001 C CNN
F 3 "" H 3900 1500 50  0001 C CNN
	1    3900 2700
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 J4
U 1 1 590101D0
P 4400 3400
F 0 "J4" H 4400 3800 50  0000 C CNN
F 1 "Main_3" V 4400 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07_Pitch2.54mm" H 4400 2200 50  0001 C CNN
F 3 "" H 4400 2200 50  0001 C CNN
	1    4400 3400
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 J2
U 1 1 59010278
P 3900 5300
F 0 "J2" H 3900 5700 50  0000 C CNN
F 1 "Function_1" V 3900 5300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07_Pitch2.54mm" H 3900 4100 50  0001 C CNN
F 3 "" H 3900 4100 50  0001 C CNN
	1    3900 5300
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 J5
U 1 1 59010306
P 4400 6100
F 0 "J5" H 4400 6500 50  0000 C CNN
F 1 "Function_2" V 4400 6100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07_Pitch2.54mm" H 4400 4900 50  0001 C CNN
F 3 "" H 4400 4900 50  0001 C CNN
	1    4400 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1700 5300 1700
Wire Wire Line
	4650 1800 5300 1800
Wire Wire Line
	4650 1900 5300 1900
Wire Wire Line
	4650 2000 5300 2000
Wire Wire Line
	4650 2100 5300 2100
Wire Wire Line
	4650 2200 5300 2200
Wire Wire Line
	4650 6400 5300 6400
Wire Wire Line
	4650 6300 5300 6300
Wire Wire Line
	4650 6200 5300 6200
Wire Wire Line
	4650 6100 5300 6100
Wire Wire Line
	4650 6000 5300 6000
Wire Wire Line
	4650 5900 5300 5900
Wire Wire Line
	4650 5800 5300 5800
Wire Wire Line
	4650 5700 5300 5700
Wire Wire Line
	4650 5700 4650 5600
Wire Wire Line
	4650 5600 4150 5600
Wire Wire Line
	5300 2300 4850 2300
Wire Wire Line
	4850 2300 4850 2400
Wire Wire Line
	4850 2400 4150 2400
Wire Wire Line
	5300 2400 4950 2400
Wire Wire Line
	4950 2400 4950 2500
Wire Wire Line
	4950 2500 4150 2500
Wire Wire Line
	5300 2600 4150 2600
Wire Wire Line
	5300 2700 4150 2700
Wire Wire Line
	5300 2800 4150 2800
Wire Wire Line
	5300 2900 4150 2900
Wire Wire Line
	4650 3100 4900 3100
Wire Wire Line
	4900 3100 4900 3000
Wire Wire Line
	4900 3000 5300 3000
Wire Wire Line
	5300 3100 5000 3100
Wire Wire Line
	5000 3100 5000 3200
Wire Wire Line
	5000 3200 4650 3200
Wire Wire Line
	5300 3200 5100 3200
Wire Wire Line
	5100 3200 5100 3300
Wire Wire Line
	5100 3300 4650 3300
Wire Wire Line
	5300 3300 5200 3300
Wire Wire Line
	5200 3300 5200 3400
Wire Wire Line
	5200 3400 4650 3400
Wire Wire Line
	5150 3500 5150 5400
Wire Wire Line
	5150 3500 4650 3500
Wire Wire Line
	5050 3600 5050 5500
Wire Wire Line
	5050 3600 4650 3600
Wire Wire Line
	6200 3550 6200 3700
Wire Wire Line
	6200 3700 5400 3700
Wire Wire Line
	5400 3700 5400 4350
Wire Wire Line
	5400 4350 3550 4350
Wire Wire Line
	3550 4350 3250 4350
Wire Wire Line
	3250 1700 3250 2400
Wire Wire Line
	3250 2400 3250 3100
Wire Wire Line
	3250 3100 3250 4350
Wire Wire Line
	3250 4350 3250 5000
Wire Wire Line
	3250 5000 3250 5800
Wire Wire Line
	3250 2400 3650 2400
Wire Wire Line
	3250 1700 4150 1700
Connection ~ 3250 2400
Wire Wire Line
	4150 3100 3250 3100
Connection ~ 3250 3100
Wire Wire Line
	3250 5000 3650 5000
Connection ~ 3250 4350
Wire Wire Line
	3250 5800 4150 5800
Connection ~ 3250 5000
Wire Wire Line
	6300 3650 6300 3800
Wire Wire Line
	6300 3800 5500 3800
Wire Wire Line
	5500 3800 5500 4450
Wire Wire Line
	5500 4450 3750 4450
Wire Wire Line
	3750 4450 3350 4450
Wire Wire Line
	3350 1800 3350 2500
Wire Wire Line
	3350 2500 3350 3200
Wire Wire Line
	3350 3200 3350 4450
Wire Wire Line
	3350 4450 3350 5100
Wire Wire Line
	3350 5100 3350 5900
Wire Wire Line
	3350 2500 3650 2500
Wire Wire Line
	4150 3200 3350 3200
Connection ~ 3350 3200
Wire Wire Line
	3350 1800 4150 1800
Connection ~ 3350 2500
Wire Wire Line
	3350 5100 3650 5100
Connection ~ 3350 4450
Wire Wire Line
	3350 5900 4150 5900
Connection ~ 3350 5100
Wire Wire Line
	4050 1900 4150 1900
Wire Wire Line
	4050 750  7400 750 
Wire Wire Line
	3550 2600 3650 2600
Wire Notes Line
	10000 2500 10000 4600
Wire Notes Line
	7700 4600 7700 2500
Wire Wire Line
	6850 6400 6850 7300
Wire Wire Line
	6850 7300 3450 7300
Wire Wire Line
	3450 7300 3450 5200
Wire Wire Line
	3450 5200 3650 5200
$Comp
L Earth #PWR1
U 1 1 5901AF10
P 2400 6400
F 0 "#PWR1" H 2400 6150 50  0001 C CNN
F 1 "Earth" H 2400 6250 50  0001 C CNN
F 2 "" H 2400 6400 50  0001 C CNN
F 3 "" H 2400 6400 50  0001 C CNN
	1    2400 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3500 4100 3500
Wire Wire Line
	4100 3500 4150 3500
Connection ~ 4100 3500
Wire Wire Line
	4100 3600 4150 3600
Wire Wire Line
	2400 2800 3600 2800
Wire Wire Line
	3600 2800 3650 2800
Connection ~ 3600 2800
Wire Wire Line
	3650 2900 3600 2900
Wire Wire Line
	2400 2100 4100 2100
Wire Wire Line
	4100 2100 4150 2100
Connection ~ 4100 2100
Wire Wire Line
	4150 2200 4100 2200
Wire Wire Line
	6300 6400 6350 6400
Wire Wire Line
	6350 6700 6350 6400
Wire Wire Line
	6350 6400 6350 6300
Connection ~ 5900 6700
Wire Wire Line
	6350 6300 6300 6300
Connection ~ 6350 6400
Wire Wire Line
	6300 6200 6350 6200
Wire Wire Line
	6350 6200 6350 4400
Connection ~ 6350 4400
Wire Wire Line
	6300 3300 6350 3300
Wire Wire Line
	6350 3300 6350 3450
Wire Wire Line
	6350 3450 6000 3450
Wire Wire Line
	6000 3450 6000 3600
Connection ~ 5900 3600
Wire Wire Line
	6350 3200 6300 3200
Wire Wire Line
	6350 1300 6350 3100
Wire Wire Line
	6350 3100 6350 3200
Connection ~ 6350 1300
Wire Wire Line
	6300 3100 6350 3100
Connection ~ 6350 3100
Text GLabel 4050 1250 2    60   Input ~ 0
Main_LED_5V
Text GLabel 3450 6800 0    60   Input ~ 0
Function_LED_5V
Text GLabel 3350 4600 2    60   Input ~ 0
SDA
Text GLabel 3250 4600 0    60   Input ~ 0
SCL
$Comp
L TEST_1P J7
U 1 1 59012F84
P 8750 1350
F 0 "J7" H 8750 1620 50  0000 C CNN
F 1 "LDR_1" H 8750 1550 50  0000 C CNN
F 2 "Connectors:1pin" H 8950 1350 50  0001 C CNN
F 3 "" H 8950 1350 50  0001 C CNN
	1    8750 1350
	-1   0    0    1   
$EndComp
$Comp
L R R6
U 1 1 590130AF
P 8300 1600
F 0 "R6" V 8380 1600 50  0000 C CNN
F 1 "10k" V 8300 1600 50  0000 C CNN
F 2 "" V 8230 1600 50  0001 C CNN
F 3 "" H 8300 1600 50  0001 C CNN
	1    8300 1600
	-1   0    0    1   
$EndComp
$Comp
L +3V3 #PWR9
U 1 1 5901325F
P 8750 850
F 0 "#PWR9" H 8750 700 50  0001 C CNN
F 1 "+3V3" H 8750 990 50  0000 C CNN
F 2 "" H 8750 850 50  0001 C CNN
F 3 "" H 8750 850 50  0001 C CNN
	1    8750 850 
	1    0    0    -1  
$EndComp
Text Notes 8600 2700 0    60   ~ 0
<-- +3V3
$Comp
L R R5
U 1 1 59013680
P 8300 1050
F 0 "R5" V 8380 1050 50  0000 C CNN
F 1 "47k" V 8300 1050 50  0000 C CNN
F 2 "" V 8230 1050 50  0001 C CNN
F 3 "" H 8300 1050 50  0001 C CNN
	1    8300 1050
	-1   0    0    1   
$EndComp
$Comp
L R R7
U 1 1 59013990
P 8300 1950
F 0 "R7" V 8380 1950 50  0000 C CNN
F 1 "2k2" V 8300 1950 50  0000 C CNN
F 2 "" V 8230 1950 50  0001 C CNN
F 3 "" H 8300 1950 50  0001 C CNN
	1    8300 1950
	-1   0    0    1   
$EndComp
$Comp
L Earth #PWR8
U 1 1 590139F2
P 8300 2200
F 0 "#PWR8" H 8300 1950 50  0001 C CNN
F 1 "Earth" H 8300 2050 50  0001 C CNN
F 2 "" H 8300 2200 50  0001 C CNN
F 3 "" H 8300 2200 50  0001 C CNN
	1    8300 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 1200 8300 1350
Wire Wire Line
	8300 1350 8300 1450
Connection ~ 8300 1350
Wire Wire Line
	8300 1750 8300 1800
Wire Wire Line
	8300 2100 8300 2200
Wire Wire Line
	8750 850  8750 900 
Wire Wire Line
	8750 900  8750 1350
Wire Wire Line
	8300 900  8750 900 
Connection ~ 8750 900 
Wire Wire Line
	5150 5400 5300 5400
Wire Wire Line
	5050 5500 5300 5500
Wire Wire Line
	4150 5000 4300 5000
Wire Wire Line
	4300 5000 4300 4800
Wire Wire Line
	4300 4800 5300 4800
Wire Wire Line
	4150 5100 4400 5100
Wire Wire Line
	4400 5100 4400 4900
Wire Wire Line
	4400 4900 5300 4900
Wire Wire Line
	5300 5000 4500 5000
Wire Wire Line
	4500 5000 4500 5200
Wire Wire Line
	4500 5200 4150 5200
Wire Wire Line
	4150 5300 4600 5300
Wire Wire Line
	4600 5300 4600 5100
Wire Wire Line
	4600 5100 5300 5100
Wire Wire Line
	5300 5200 4700 5200
Wire Wire Line
	4700 5200 4700 5400
Wire Wire Line
	4700 5400 4150 5400
Wire Wire Line
	5300 5300 4800 5300
Wire Wire Line
	4800 5300 4800 5500
Wire Wire Line
	4800 5500 4150 5500
Wire Wire Line
	3650 5300 3550 5300
Wire Wire Line
	3550 5300 3550 6000
Wire Wire Line
	3550 6000 4150 6000
Wire Wire Line
	3650 5400 3650 5500
Connection ~ 3650 5500
Wire Wire Line
	4050 750  4050 1900
Wire Wire Line
	2400 2100 2400 2800
Wire Wire Line
	2400 2800 2400 3500
Wire Wire Line
	2400 3500 2400 5500
Wire Wire Line
	2400 5500 2400 6300
Wire Wire Line
	2400 6300 2400 6400
Wire Wire Line
	4150 2000 3550 2000
Wire Wire Line
	3550 2000 3550 2600
Connection ~ 2400 2800
Wire Wire Line
	3650 2700 3450 2700
Wire Wire Line
	3450 2700 3450 3300
Wire Wire Line
	3450 3300 4150 3300
$Comp
L LogicLevelShifter U4
U 1 1 59026C23
P 8850 5050
F 0 "U4" H 8850 5300 60  0000 C CNN
F 1 "LogicLevelShifter" H 8850 4800 60  0000 C CNN
F 2 "cubetto_clone:logiclevelShifter" H 8850 5050 60  0001 C CNN
F 3 "" H 8850 5050 60  0001 C CNN
	1    8850 5050
	1    0    0    -1  
$EndComp
Wire Notes Line
	7700 2500 10000 2500
Wire Notes Line
	10000 4600 7700 4600
Wire Wire Line
	9550 3450 9800 3450
Wire Wire Line
	7400 750  7400 5000
Wire Wire Line
	7850 3850 7950 3850
Wire Wire Line
	7850 3850 7850 4900
$Comp
L Witty(ESP-12F) U3
U 1 1 58F4EFC5
P 8750 3500
F 0 "U3" H 8750 3950 60  0000 C CNN
F 1 "Witty(ESP-12F)" H 8750 3050 60  0000 C CNN
F 2 "Cubetto:witty" H 8150 2650 60  0001 C CNN
F 3 "" H 8150 2650 60  0000 C CNN
	1    8750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4900 7850 4900
Wire Wire Line
	7850 4900 8400 4900
Wire Wire Line
	9800 3450 9800 5100
Wire Wire Line
	9800 5100 9300 5100
$Comp
L Earth #PWR11
U 1 1 590294C0
P 9400 5500
F 0 "#PWR11" H 9400 5250 50  0001 C CNN
F 1 "Earth" H 9400 5350 50  0001 C CNN
F 2 "" H 9400 5500 50  0001 C CNN
F 3 "" H 9400 5500 50  0001 C CNN
	1    9400 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5200 9400 5200
Wire Wire Line
	9400 5200 9400 5450
Wire Wire Line
	9400 5450 9400 5500
Wire Wire Line
	8400 5200 8400 5450
Wire Wire Line
	8400 5450 9400 5450
Connection ~ 9400 5450
$Comp
L +3V3 #PWR10
U 1 1 590297FE
P 9400 4800
F 0 "#PWR10" H 9400 4650 50  0001 C CNN
F 1 "+3V3" H 9400 4940 50  0000 C CNN
F 2 "" H 9400 4800 50  0001 C CNN
F 3 "" H 9400 4800 50  0001 C CNN
	1    9400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4800 9400 4900
Wire Wire Line
	9400 4900 9300 4900
Wire Wire Line
	7500 6400 6850 6400
Wire Wire Line
	7500 5100 7500 6400
Wire Wire Line
	7400 5000 8400 5000
Wire Wire Line
	8400 5100 7500 5100
Text GLabel 9800 4700 2    60   Input ~ 0
Function_LED_3V3
Text GLabel 9800 4900 2    60   Input ~ 0
Main_LED_3V3
Wire Wire Line
	7950 3250 7850 3250
Wire Wire Line
	7850 3250 7850 1350
Wire Wire Line
	7850 1350 8300 1350
$Comp
L R R1
U 1 1 59061581
P 3550 4100
F 0 "R1" V 3630 4100 50  0000 C CNN
F 1 "4.7k" V 3550 4100 50  0000 C CNN
F 2 "" V 3480 4100 50  0001 C CNN
F 3 "" H 3550 4100 50  0001 C CNN
	1    3550 4100
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 59061C61
P 3750 4100
F 0 "R2" V 3830 4100 50  0000 C CNN
F 1 "4.7k" V 3750 4100 50  0000 C CNN
F 2 "" V 3680 4100 50  0001 C CNN
F 3 "" H 3750 4100 50  0001 C CNN
	1    3750 4100
	-1   0    0    1   
$EndComp
$Comp
L +3V3 #PWR2
U 1 1 590625BD
P 3650 3900
F 0 "#PWR2" H 3650 3750 50  0001 C CNN
F 1 "+3V3" H 3650 4040 50  0000 C CNN
F 2 "" H 3650 3900 50  0001 C CNN
F 3 "" H 3650 3900 50  0001 C CNN
	1    3650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3950 3650 3950
Wire Wire Line
	3650 3950 3750 3950
Wire Wire Line
	3650 3900 3650 3950
Connection ~ 3650 3950
Wire Wire Line
	3550 4250 3550 4350
Connection ~ 3550 4350
Wire Wire Line
	3750 4250 3750 4450
Connection ~ 3750 4450
Wire Wire Line
	3650 5500 2400 5500
Connection ~ 2400 5500
Wire Wire Line
	3600 2900 3600 2800
Wire Wire Line
	4100 2200 4100 2100
Wire Wire Line
	4150 6300 4100 6300
Wire Wire Line
	4100 6300 2400 6300
Connection ~ 2400 6300
Wire Wire Line
	4150 2300 2700 2300
Wire Wire Line
	2700 2300 2700 3000
Wire Wire Line
	2700 3000 2700 3700
Wire Wire Line
	2700 3700 2700 5600
Wire Wire Line
	2700 5600 2700 6400
Wire Wire Line
	2700 3000 3650 3000
Wire Wire Line
	2700 3700 4150 3700
Connection ~ 2700 3000
Wire Wire Line
	2700 5600 3650 5600
Connection ~ 2700 3700
Wire Wire Line
	2700 6400 3800 6400
Wire Wire Line
	3800 6400 4150 6400
Connection ~ 2700 5600
Wire Wire Line
	7200 4900 7200 6300
Wire Wire Line
	7200 6300 6700 6300
Wire Wire Line
	6700 6300 6700 7200
Wire Wire Line
	6700 7200 3800 7200
Wire Wire Line
	3800 7200 3800 6400
Connection ~ 3800 6400
Connection ~ 7850 4900
Text GLabel 2700 4100 2    60   Input ~ 0
5V
Wire Wire Line
	4100 3500 4100 3600
Connection ~ 2400 3500
Wire Wire Line
	4150 6200 4100 6200
Wire Wire Line
	4100 6200 4100 6300
Connection ~ 4100 6300
Text Notes 7850 5900 0    60   ~ 0
Reworked LED driving, because of GPIO15 hard to use pin.\nDo rework the 5V side.
Connection ~ 8000 5150
Connection ~ 8000 5700
Wire Wire Line
	8000 5700 8000 5150
$EndSCHEMATC
