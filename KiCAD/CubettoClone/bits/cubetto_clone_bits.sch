EESchema Schematic File Version 4
LIBS:cubetto_clone_bits-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L hn58x2432:HN58X2432 U1
U 1 1 58CE5C5A
P 4600 3100
F 0 "U1" H 4350 3350 50  0000 C CNN
F 1 "HN58X2432" H 4900 3350 50  0001 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4650 2850 50  0001 L CNN
F 3 "" H 4600 3000 50  0001 C CNN
	1    4600 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3000 4050 3000
Wire Wire Line
	4050 3000 4050 3100
Wire Wire Line
	4050 3100 4200 3100
Wire Wire Line
	4050 3200 4200 3200
Connection ~ 4050 3100
Wire Wire Line
	4050 3400 4600 3400
Connection ~ 4050 3200
Wire Wire Line
	4600 2800 5200 2800
Connection ~ 4600 3400
Connection ~ 5200 2800
Wire Wire Line
	5000 3000 5200 3000
Wire Wire Line
	5200 3000 5200 2800
Wire Wire Line
	5000 3100 5650 3100
Wire Wire Line
	5000 3200 5650 3200
$Comp
L conn:TEST_1P J2
U 1 1 58CE606A
P 5650 3100
F 0 "J2" V 5650 3297 50  0000 C CNN
F 1 "SCL" V 5650 3454 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 5850 3100 50  0001 C CNN
F 3 "" H 5850 3100 50  0001 C CNN
	1    5650 3100
	0    1    1    0   
$EndComp
$Comp
L conn:TEST_1P J3
U 1 1 58CE60A1
P 5650 3200
F 0 "J3" V 5650 3397 50  0000 C CNN
F 1 "SDA" V 5650 3554 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 5850 3200 50  0001 C CNN
F 3 "" H 5850 3200 50  0001 C CNN
	1    5650 3200
	0    1    1    0   
$EndComp
$Comp
L conn:TEST_1P J1
U 1 1 58CE60E0
P 5650 2800
F 0 "J1" V 5650 2997 50  0000 C CNN
F 1 "+5V" V 5650 3154 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 5850 2800 50  0001 C CNN
F 3 "" H 5850 2800 50  0001 C CNN
	1    5650 2800
	0    1    1    0   
$EndComp
$Comp
L conn:TEST_1P J4
U 1 1 58CE611F
P 5650 3400
F 0 "J4" V 5650 3597 50  0000 C CNN
F 1 "GND" V 5650 3754 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 5850 3400 50  0001 C CNN
F 3 "" H 5850 3400 50  0001 C CNN
	1    5650 3400
	0    1    1    0   
$EndComp
Text Label 5250 3400 0    60   ~ 0
GND
Text Label 5250 2800 0    60   ~ 0
+5V
Text Label 5250 3100 0    60   ~ 0
SCL
Text Label 5250 3200 0    60   ~ 0
SDA
Wire Wire Line
	4050 3100 4050 3200
Wire Wire Line
	4050 3200 4050 3400
Wire Wire Line
	4600 3400 5650 3400
Wire Wire Line
	5200 2800 5650 2800
$EndSCHEMATC
