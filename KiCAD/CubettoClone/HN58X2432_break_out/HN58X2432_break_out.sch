EESchema Schematic File Version 4
LIBS:HN58X2432_break_out-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L hn58x2432:HN58X2432 U1
U 1 1 5984D5E6
P 5650 3700
F 0 "U1" H 5400 3950 50  0000 C CNN
F 1 "HN58X2432" H 5950 3950 50  0001 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5700 3450 50  0001 L CNN
F 3 "" H 5650 3600 50  0001 C CNN
	1    5650 3700
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X04 J1
U 1 1 5984D7BD
P 4900 3750
F 0 "J1" H 4900 4000 50  0000 C CNN
F 1 "CONN_01X04" V 5000 3750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 4900 3750 50  0001 C CNN
F 3 "" H 4900 3750 50  0001 C CNN
	1    4900 3750
	-1   0    0    1   
$EndComp
$Comp
L conn:CONN_01X04 J2
U 1 1 5984D7F0
P 6500 3650
F 0 "J2" H 6500 3900 50  0000 C CNN
F 1 "CONN_01X04" V 6600 3650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6500 3650 50  0001 C CNN
F 3 "" H 6500 3650 50  0001 C CNN
	1    6500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3600 5250 3600
Wire Wire Line
	5100 3700 5250 3700
Wire Wire Line
	5100 3800 5250 3800
Wire Wire Line
	5650 4000 5250 4000
Wire Wire Line
	5250 4000 5250 3900
Wire Wire Line
	5250 3900 5100 3900
Wire Wire Line
	6050 3800 6300 3800
Wire Wire Line
	6050 3700 6300 3700
Wire Wire Line
	6050 3600 6300 3600
Wire Wire Line
	5650 3400 6050 3400
Wire Wire Line
	6050 3400 6050 3500
Wire Wire Line
	6050 3500 6300 3500
$EndSCHEMATC
