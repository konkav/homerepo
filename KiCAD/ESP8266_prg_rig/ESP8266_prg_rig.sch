EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP-12F U1
U 1 1 5B77E2A2
P 5800 3350
F 0 "U1" H 5800 4328 50  0000 C CNN
F 1 "ESP-12F" H 5800 4237 50  0000 C CNN
F 2 "ESP8266_prg_rig:ESP-12E_SMD" H 5800 3350 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 5450 3450 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
