This is a sketch for ESP8266. It is a remote console for my Orange Pi "server". It uses a serial pinswap (TX is GPIO15, RX is GPIO13). Connect it to a SBC serial, and connect to is over telnet.
It need a sensitive.h file with the content below, or just include it in the sketch in place of the file include:

const char ssid[] = "";
const char pass[] = "";
IPAddress IP(0, 0, 0, 0);
IPAddress subnet(0, 0, 0, 0);
IPAddress gateway(0, 0, 0, 0);
byte port = 0;

OTA update included.