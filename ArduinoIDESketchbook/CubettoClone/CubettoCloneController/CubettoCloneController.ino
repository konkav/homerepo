// #include <pgmspace.h>

// MCP23016
#include <Wire.h>
#define MCP1 0x20 // B0100+A2+A1+A0 => B0100000
#define MCP2 0x21 // B0100+A2+A1+A0 => B0100001
#define GP0 0x00 // PORT0 first 8 relays
#define GP1 0x01 // PORT1 second 8 relays
//#define OLAT0 0x02 // LATCH0 command to write on latch 0 first 8 relays
//#define OLAT1 0x03 // LATCH1 command to write on latch 1 second 8 relays
//#define IPOL0 0x04 // INPUT POLARITY PORT REGISTER 0
//#define IPOL1 0x05 // INPUT POLARITY PORT REGISTER 1
#define IODIR0 0x06 // I/O DIRECTION REGISTER 0
#define IODIR1 0x07 // I/O DIRECTION REGISTER 1
//#define INTCAP0 0x08 // INTERRUPT CAPTURE REGISTER 0 (Read Only)
//#define INTCAP1 0x09 // INTERRUPT CAPTURE REGISTER 1 (Read Only)
//#define IOCON0 0x0A // I/O EXPANDER CONTROL REGISTER 0
//#define IOCON1 0x0B // I/O EXPANDER CONTROL REGISTER 1

//I2C EEPROM HN58X2432 32kbit, AT24C32 clone
#define EEP 0x50 // B1010000 - 1010+A2+A1+A0
#define EEADDRHI 0x00 // 0000abcd
#define EEADDRLO 0x0A // efghijkl - 12bit address space 0-4095 - default is 10 decimal
const byte BL = 1; //LEFT
const byte BR = 2; //RIGHT
const byte BF = 3; //FORWARD
const byte BA = 4; //RANDOM
const byte BS1 = 5; //SUBROUTINE 1
const byte BS2 = 6; //SUBROUTINE 2

// WS2812B
#define FASTLED_ESP8266_RAW_PIN_ORDER
#define BRIGHTNESS 50
#include <FastLED.h>
const byte NUM_LEDS = 4;
const byte DATA_PIN = 4;
CRGB leds[NUM_LEDS];
CRGB ledoff; // no bit in place
CRGB lederror; // out of range value
CRGB ledruning; // running bit
CRGB ledok; // OK bit
CRGB ledprg; // programming bit
CRGB ledbits[7]; // = {CRGB::White , CRGB::Gold, CRGB::Red, CRGB::Green , CRGB::Blue , CRGB::Purple ,  CRGB::Orange};
//                        0              1          2           3            4             5                6
//                      other          left       right      forward       random         sub1             sub2
//  LED colors for programming, same colors as the bits


// the emulated EEPROM for storing settings, which need to survive reboot, like color settings for LED, wifi setup, etc.
#include <EEPROM.h>
const byte config_eeprom_size = 94; // size of the config EEPROM


// WiFi
#include <ESP8266WiFi.h>
IPAddress robotIP;
IPAddress controllerIP;
IPAddress subnet;
IPAddress gateway;
char APssid[20];
char APpass[20];
byte APchannel;
byte port = 23;
WiFiServer wifiserver(23);
WiFiClient robot;



byte i2cSDA = 12;
byte i2cSCL = 14;
unsigned int i2cSPEED = 100000;
byte ledchange = 0; // semaphore for pushing out the LED refresh
int re = 0;
byte we = 0;
byte a = 0;
byte menu = 99;
byte ewa = MCP1; // expander_write_address
byte ewr = GP0; // expander_write_register
byte ewv0 = 0; // expander_write_value1
byte ewv1 = 0; // expander_write_value2
char inpt = 0; // input from serial
byte bitsvalue[32] = {0}; // storage for the recognised program bits. constantly updated, until execution started
byte prgstack[20]; // no, there is not going to be unlimited subrouting jumping
byte prgrun = 99; //semaphore for the program running
byte rewr = 0; // read/write semaphore for the reading/writing eeproms : 0 - main loop, 1 - read, 2 - write
byte prgbit = 0; // programming place to write next
byte prgbitmin = 0; // place of the first bit inserted
byte bitplacecounter = 0;  // the bits (EEPROM) selector
int bitplacevalue = 0; // the bits (EEPROM) value
byte bitplace[32][3] {  // portexpander values for different places
  {MCP1, 1, 0},
  {MCP1, 2, 0},
  {MCP1, 4, 0},
  {MCP1, 8, 0},
  {MCP1, 16, 0},
  {MCP1, 32, 0},
  {MCP1, 64, 0},
  {MCP1, 128, 0},
  {MCP1, 0, 1},
  {MCP1, 0, 2},
  {MCP1, 0, 4},
  {MCP1, 0, 8},
  {MCP1, 0, 16},
  {MCP1, 0, 32},
  {MCP1, 0, 64},
  {MCP1, 0, 128},
  {MCP2, 1, 0},
  {MCP2, 2, 0},
  {MCP2, 4, 0},
  {MCP2, 8, 0},
  {MCP2, 16, 0},
  {MCP2, 32, 0},
  {MCP2, 64, 0},
  {MCP2, 128, 0},
  {MCP2, 0, 1},
  {MCP2, 0, 2},
  {MCP2, 0, 4},
  {MCP2, 0, 8},
  {MCP2, 0, 16},
  {MCP2, 0, 32},
  {MCP2, 0, 64},
  {MCP2, 0, 128}
};
byte seq; // sequence nr.
byte seqsync; // semaphore for seq sync
byte robocomm = 0; // semaphore for communication
byte msgend = 85; //msg terminator
byte nextstep = 0; // semaphore for sending state
byte roboid[4]; // controller ID received in init msg
byte robomsg[10]; // message cache, robot response
byte ctrlmsg[10] = {0}; // message cache, controllers will send this
// 0,1,2,3 - robot/ctrl ID , 4 - who? 2=ctrl 1=robot, 5 - unused, 6 - sequence nr., 7 - command, 8 -command data, 9 - msgend
// 7 (8) - command: 1 - init msg (1-robo->ctrl,2-ctrl->robo), 2 - sync sequence nr.(1-ctrl request,2-robo reply), 3 - self data (battery voltage, etc.), 4 - robot led, 5 - movement data
// robocomm: 0 - no connection, 1 - IP connected, comm init sending, 2 - comm init received, 3 - command requested, 4 - command arrived



void sendmsg() {
  Serial.println("Sending message:");
  for (byte x = 0; x < 10; x++) {
    Serial.print(x);
    Serial.print(". byte: ");
    Serial.println(ctrlmsg[x]);
    robot.write(ctrlmsg[x]);
  }
  Serial.println();
}

int read_bits_eeprom() {

  Wire.beginTransmission(EEP);
  Wire.write(EEADDRHI);
  Wire.write(EEADDRLO);
  re = Wire.endTransmission();

  if (re != 0) return (0 - re);

  delay(10);

  Wire.requestFrom(EEP, 1);
  re = Wire.read();
  return re;
}

byte write_bits_eeprom(byte b) {

  Wire.beginTransmission(EEP);
  Wire.write(EEADDRHI);
  Wire.write(EEADDRLO);
  Wire.write(b);
  Wire.endTransmission();

  delay(10);

  we = 0; //ERROR
  we = read_bits_eeprom();

  if (we == b) {
    return 1; //TRUE
  } else {
    return 2; //FALSE
  }
}

void expander_write(byte ewa, byte ewr, byte ewv0, byte ewv1) {

  Wire.beginTransmission(ewa);
  Wire.write(ewr);
  Wire.write(ewv0);
  Wire.write(ewv1);
  Wire.endTransmission();
}

void reset_config() {

  byte config_data[config_eeprom_size] = {
    // 36 bytes for LED colors, 12 colors * 3 byte
    0x00, 0x00, 0x00,     // ledoff
    0xFF, 0x00, 0x00,     // lederror
    0x80, 0x00, 0x80,     // ledruning
    0x00, 0x80, 0x00,     // ledok
    0x69, 0x69, 0x69,     // ledprg
    0xFF, 0xFF, 0xFF,     // ledbits other
    0xFF, 0xD7, 0x00,     // ledbits left
    0xFF, 0x00, 0x00,     // ledbits right
    0x00, 0x80, 0x00,     // ledbits forward
    0x00, 0x00, 0xFF,     // ledbits random
    0x80, 0x00, 0x80,     // ledbits sub1
    0xFF, 0xA5, 0x00,     // ledbits sub2
    // 16 bytes for IP config, 4 data * 4 bytes
    10, 10, 10, 10,       // IP address
    255, 255, 255, 0,     // netmask
    10, 10, 10, 1,        // gateway
    10, 10, 10, 20,       // robot IP address
    // 1 byte for wifi channel settings
    10                    // channel, 1-13
    // alltogether 53 bytes
  };

  char newAPssid[20] = "CubettoClone";
  char newAPpass[20] = "asdfghjk";

  for (byte x = 0; x < 20; x++) {
    config_data[(x + 53)] = (byte)newAPssid[x];
  }

  for (byte x = 0; x < 20; x++) {
    config_data[(x + 73)] = (byte)newAPpass[x];
  }

  config_data[(config_eeprom_size - 1)] = 85;


  for (byte x = 0; x < 94; x++) EEPROM.write(x, config_data[x]);

  EEPROM.commit();
  Serial.println("Controller config reset.");
}

void initialize_controller() {

  // 0-35    values for LED colors
  // 36-51   values for IP addresses
  // 52      WiFi channel
  // 53-72   SSID
  // 73-92   PSK
  // 93      initialization value: decimal 85 = config is initialised, other value = invoke reset config

  if (EEPROM.read(config_eeprom_size - 1) != 85) {
    Serial.println("\n\n\nUNINITIALISED CONTROLLER !!!  Invoking controller config reset...");
    reset_config();
  }

  ledoff = CRGB(EEPROM.read(0), EEPROM.read(1), EEPROM.read(2));
  lederror = CRGB(EEPROM.read(3), EEPROM.read(4), EEPROM.read(5));
  ledruning = CRGB(EEPROM.read(6), EEPROM.read(7), EEPROM.read(8));
  ledok = CRGB(EEPROM.read(9), EEPROM.read(10), EEPROM.read(11));
  ledprg = CRGB(EEPROM.read(12), EEPROM.read(13), EEPROM.read(14));

  for (byte x = 0; x < 7; x++) {
    ledbits[x] = CRGB(EEPROM.read(15 + (x * 3)), EEPROM.read(16 + (x * 3)), EEPROM.read(17 + (x * 3)));
  }

  controllerIP = IPAddress(EEPROM.read(36), EEPROM.read(37), EEPROM.read(38), EEPROM.read(39));
  subnet = IPAddress(EEPROM.read(40), EEPROM.read(41), EEPROM.read(42), EEPROM.read(43));
  gateway = IPAddress(EEPROM.read(44), EEPROM.read(45), EEPROM.read(46), EEPROM.read(47));
  robotIP = IPAddress(EEPROM.read(48), EEPROM.read(49), EEPROM.read(50), EEPROM.read(51));
  APchannel = EEPROM.read(52);

  for (byte x = 0; x < 20; x++) {
    APssid[x] = (char)EEPROM.read(x + 53);
  }

  for (byte x = 0; x < 20; x++) {
    APpass[x] = (char)EEPROM.read(x + 73);
  }

  wifiserver.stop();
  WiFi.persistent(false);
  WiFi.disconnect();
  WiFi.softAPdisconnect();
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(controllerIP, gateway, subnet);
  WiFi.softAP(APssid, APpass, APchannel);
  wifiserver.begin();
  wifiserver.setNoDelay(true);

  Serial.println("\nController setup done.");
}




void setup() {

  Serial.begin ( 115200 );
  delay(100);
  Serial.println("\n\nInitializing...");
  Serial.println(ESP.getFreeHeap());
  Serial.println(ESP.getSketchSize());
  Serial.println(ESP.getFreeSketchSpace());
  Serial.println(ESP.getCoreVersion());
  Serial.println(ESP.getSdkVersion());
  Serial.println(ESP.getBootVersion());

  Wire.setClock(i2cSPEED);
  Wire.begin(i2cSDA, i2cSCL);

  Wire.beginTransmission(MCP1); // setup out direction registers
  Wire.write(IODIR0); // pointer
  Wire.write(0x00); // Port0 all output
  Wire.write(0x00); // Port1 all output
  Wire.endTransmission();

  Wire.beginTransmission(MCP2); // setup out direction registers
  Wire.write(IODIR0); // pointer
  Wire.write(0x00); // Port0 all output
  Wire.write(0x00); // Port1 all output
  Wire.endTransmission();

  expander_write(MCP1, GP0, 0, 0);
  // ATM there is no second port expander -------------   expander_write(MCP2, GP0, 0, 0);

  pinMode(DATA_PIN, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness( BRIGHTNESS );
  FastLED.clear();

  // for testing the LED communication only, remove before flight
  for (byte x = 0; x < NUM_LEDS; x++) {
    leds[x] = CRGB(random(50), random(50), random(50));
  }

  FastLED.show();
  delay(50);
  ledchange = 1;

  EEPROM.begin(config_eeprom_size); // emulated EEPROM

  initialize_controller();

  randomSeed(micros());
  for (byte x = 0; x < 4; x++) {
    ctrlmsg[x] = random(256);
  }
  ctrlmsg[4] = 2;
  ctrlmsg[9] = msgend;

  //testing comm !!!@@@!!!
  ctrlmsg[0] = 66;
  ctrlmsg[1] = 77;
  ctrlmsg[2] = 88;
  ctrlmsg[3] = 99;

  delay(50);
}




void loop() {

  if (menu == 99) {
    Serial.println("\n\n\nMenu:");
    Serial.println("1 - Read/Write EEPROM(s)");
    Serial.println("2 - Setup LEDs");
    Serial.println("3 - Setup WiFi");
    Serial.println("4 - Show connected robot");
    Serial.println("5 - Reset config to default values");
    Serial.println("6 - Robot manual drive");
    Serial.println("7 - ");
    Serial.println("8 - ");
    Serial.println("9 - ");
    Serial.println("0 - ");
    Serial.println();
    menu = 98;
  }


  // *******************  START SERIAL READ IF
  if (Serial.available() > 0) {
    inpt = Serial.read();

    if (menu == 90) {
      if  (inpt == 'f' || inpt == 'F') {
        Serial.print("  Send Forward  ");
        nextstep = BF;
      }
      if  (inpt == 'l' || inpt == 'L') {
        Serial.print("  Send Left  ");
        nextstep = BL;
      }
      if  (inpt == 'r' || inpt == 'R') {
        Serial.print("  Send Right  ");
        nextstep = BR;
      }
      if  (inpt == 'q' || inpt == 'Q') {
        Serial.println("Quitting manual drive mode.");
        Serial.println();
        menu = 99;
      }
    }

    if (menu == 92) {
      if  (inpt == 'r' || inpt == 'R') {
        Serial.println();
        Serial.println("Reseting... ");
        Serial.println();
        reset_config();
        initialize_controller();
        menu = 99;
      }

      if  (inpt == 'q' || inpt == 'Q') {
        menu = 99;
      }
    }

    if (menu == 94) {
      if  (inpt == 'l' || inpt == 'L') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BL);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("LEFT programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == 'r' || inpt == 'R') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BR);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("RIGHT programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == 'f' || inpt == 'F') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BF);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("FORWARD programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == 'a' || inpt == 'A') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BA);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("RANDOM programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == '1') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BS1);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("SUB - 1 programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == '2') {
        expander_write(bitplace[prgbit][0], GP0 , bitplace[prgbit][1], bitplace[prgbit][2]);
        delay(10);
        a = write_bits_eeprom(BS2);
        if (a == 1) {
          leds[prgbit] = ledbits[1];
          ledchange = 1;
          Serial.print("SUB - 2 programmed in position ");
          Serial.println(prgbit);
        } else {
          Serial.print("Programming FAILED !!! in position ");
          Serial.println(prgbit);
        }
      }
      if  (inpt == 'q' || inpt == 'Q') {
        Serial.println("Quiting to main menu.....");
        Serial.println();
        rewr = 0;
        bitplacecounter = 0;
        prgbit = 0;
        prgbitmin = 32;
        FastLED.clear();
        ledchange = 1;
        menu = 99;
      }
      if (prgbitmin != 32 && prgbitmin == prgbit) {
        Serial.println("Last bit programmed. Going back to Miami.....");
        Serial.println();
        rewr = 0;
        bitplacecounter = 0;
        prgbit = 0;
        prgbitmin = 32;
        FastLED.clear();
        ledchange = 1;
        menu = 99;
      }
      if (prgbitmin != 32 && prgbit < prgbitmin) prgbit = prgbitmin;


    }

    if (menu == 96) {
      if (inpt == 'q' or inpt == 'Q') {
        rewr = 0;
        bitplacecounter = 0;
        prgbitmin = 32;
        FastLED.clear();
        ledchange = 1;
        menu = 99;
      }
      if (inpt == 's' or inpt == 'S') {
        rewr = 2;
        //if (prgbitmin != 32) prgbit = prgbitmin;
        Serial.println("Brown LED shows the next bit to be programmed. For the programming you can use the next keys: ");
        Serial.println("[L]eft, [R]ight, [F]orward, r[A]ndom, sub[1], sub[2], [Q]uit");
        Serial.println("Any other input will skip to the next bit.");
        menu = 94;
      }
    }

    if (menu == 98) {
      switch (inpt) {
        /*case 0:
          menu = 97;
          break;*/
        case '1':
          Serial.println();
          Serial.println("EEPROM programming mode. All bits are read out. LEDs are colored by the bits color. If bit is recognized, but value is out of range, it is signed white.");
          Serial.println("Put in all the bits for programming, and wait until all the bits are recognised. The programming will start at the first recognised bit.");
          Serial.println();
          Serial.println("Enter [S] for start of programming, or [Q] to quit programming mode.");
          Serial.println();
          rewr = 1;
          bitplacecounter = 0;
          prgbit = 0;
          prgbitmin = 32;
          FastLED.clear();
          ledchange = 1;
          menu = 96;
          break;
        case '2':
          Serial.print("Setup LEDs...   ");
          menu = 99;
          break;
        case '3':
          Serial.print("Setup WiFi...   ");
          menu = 99;
          break;
        case '4':
          Serial.print("Show connected robot(s)...   ");
          menu = 99;
          break;
        case '5':
          Serial.println("Reseting config to default values...   ");
          Serial.println("This will reset all the custom values. Press (R)eset or (Q)uit");
          Serial.println();
          menu = 92;
          break;
        case '6':
          Serial.println();
          Serial.println("Driving robot on manual.");
          Serial.println("(F)orward, (L)eft, (R)ight, (Q)uit to main menu. Thats is.");
          Serial.println();
          menu = 90;
          break;
        default:
          Serial.print("Input value :   ");
          Serial.println(inpt);
          menu = 99;
          break;
      }
    }


  }
  // *******************  END SERIAL READ IF

  while (Serial.available()) Serial.read(); // empty serial buffer



  if (bitplacecounter > (NUM_LEDS - 1)) {
    bitplacecounter = 0;
    prgbitmin = 32;
  }

  expander_write(bitplace[bitplacecounter][0], GP0 , bitplace[bitplacecounter][1], bitplace[bitplacecounter][2]);
  delay(10);
  bitplacevalue = read_bits_eeprom();
  delay(50);

  if (bitplacevalue < 0 && leds[bitplacecounter] != ledoff) {
    leds[bitplacecounter] = ledoff;
    ledchange = 1;
    bitsvalue[bitplacecounter] = 0;
  }

  if (bitplacevalue == 0) {
    if (rewr == 0 && leds[bitplacecounter] != lederror) {
      leds[bitplacecounter] = lederror;
      ledchange = 1;
    }
    if ((rewr == 1 || rewr == 2) && leds[bitplacecounter] != ledbits[bitplacevalue]) {
      leds[bitplacecounter] = ledbits[bitplacevalue];
      ledchange = 1;
    }
    bitsvalue[bitplacecounter] = 0;
  }

  if (bitplacevalue >= 1 && bitplacevalue <= 6) {
    if (rewr == 0 && leds[bitplacecounter] != ledok) {
      leds[bitplacecounter] = ledok;
      ledchange = 1;
    }
    if (rewr == 1 && leds[bitplacecounter] != ledbits[bitplacevalue]) {
      leds[bitplacecounter] = ledbits[bitplacevalue];
      ledchange = 1;
    }
    if (rewr == 2 && leds[bitplacecounter] != ledbits[bitplacevalue] && bitplacecounter != prgbit) {
      leds[bitplacecounter] = ledbits[bitplacevalue];
      ledchange = 1;
    }
    if (bitplacecounter < prgbitmin && bitplacecounter > prgbit ) prgbitmin = bitplacecounter;
    bitsvalue[bitplacecounter] = bitplacevalue;
  }

  if (bitplacevalue > 6) {
    if (rewr == 0 && leds[bitplacecounter] != lederror) {
      leds[bitplacecounter] = lederror;
      ledchange = 1;
    }
    if ((rewr == 1 || rewr == 2) && leds[bitplacecounter] != ledbits[0]) {
      leds[bitplacecounter] = ledbits[0];
      ledchange = 1;
    }
    bitsvalue[bitplacecounter] = 0;
  }


  if (rewr == 2 && (leds[prgbit] != ledprg)) {
    leds[prgbit] = ledprg;
    ledchange = 1;
  }

  if (ledchange > 0) {
    FastLED.show();
    ledchange = 0;
    delay(50);
  }



  if (seq > 240) { // don't run out of 1 byte seq.
    seqsync = 0;
    robocomm = 3;
  }


  if (robocomm == 5) { // reading response
    if (memcmp(robomsg, roboid, 4) != 0) {
      Serial.println("Wrong roboid.");
    } else {
      if (robomsg[4] == 1 && robomsg[7] == 2 && robomsg[8] == 2) {
        Serial.println("Sequence synced.");
        seq = robomsg[6];
        seqsync = 1;
        robocomm = 3;
      }
      if (robomsg[4] == 1 && robomsg[7] == 5) {
        if (robomsg[8] == nextstep && robomsg[6] == seq) {
          Serial.println("Move reply OK.");
          nextstep = 0;
          robocomm = 3;
        } else {
          Serial.println("Move reply WRONG (nextstep or seq).");
          robocomm = 3;
          // todo: handle the wrong input. resend or something
        }
      }
    }
  }

  if (robocomm == 3) { // sending request
    if (seqsync == 0) { // resync seq.
      ctrlmsg[7] = 2;
      ctrlmsg[8] = 1;
      sendmsg();
      robocomm = 4;
      seqsync = 1;
    }
    if (nextstep > 0 && nextstep < 7) {
      seq++;
      ctrlmsg[6] = seq;
      ctrlmsg[7] = 5;
      ctrlmsg[8] = nextstep;
      sendmsg();
      //nextstep = 0;
      Serial.println("Move command sent.");
      robocomm = 4;
    }
    //------------------------------
  }

  if (robocomm == 2) {
    if (robomsg[4] == 1 && robomsg[7] == 1 && robomsg[8] == 1 ) {
      Serial.println("Robot ID:");
      for (byte x = 0; x < 4; x++) {
        roboid[x] = robomsg[x];
        Serial.print(roboid[x]);
      }
      Serial.println();
      Serial.println("Sending init comm answer.");
      ctrlmsg[6] = seq;
      ctrlmsg[7] = 1;
      ctrlmsg[8] = 2;
      // This will send the answer to robot
      sendmsg();
      robocomm = 3;
    } else {
      robocomm = 1; // wrong answer
      Serial.println("Init comm expected. Wrong msg. Waiting for good msg.");
    }
  }

  if (wifiserver.hasClient()) {          // TRUE when client connects
    if (robot) robot.stop();
    robot = wifiserver.available();
    seq = 1;
    seqsync = 0;
    robocomm = 1; // initialize the comm
  }

  if ((robot.available() > 9) && (robocomm == 1 || robocomm == 4)) {
    Serial.print("Msg from robot. Bytes available: ");
    Serial.println(robot.available());
    for (byte y = 0; y < 10; y++) {
      robomsg[y] = robot.read();
      Serial.print(robomsg[y]);
    }
    Serial.println();
    if (robomsg[9] == msgend) {
      Serial.println("Msg ended OK.");
      robocomm++;
    }
    //while (robot.available()) robot.read(); // empty IP buffer
    //Serial.write(ctrlmsg[0]);
  }


  // 0,1,2,3 - robot/ctrl ID , 4 - who? 2=ctrl 1=robot, 5 - unused, 6 - sequence nr., 7 - command, 8 -command data, 9 - msgend
  // 7 (8) - command: 1 - init msg (1-robo->ctrl,2-ctrl->robo), 2 - sync sequence nr.(1-ctrl request,2-robo reply), 3 - self data (battery voltage, etc.), 4 - robot led, 5 - movement data
  // robocomm: 0 - no connection, 1 - IP connected, comm init waiting, 2 - comm init received, 3 - command requested, 4 - command arrived

  bitplacecounter++;

  //WiFi.printDiag(Serial);
  //Serial.println(WiFi.status());
  //Serial.println(wifiserver.available());
  //Serial.println(WiFi.softAPgetStationNum());
  //Serial.println(ESP.getFreeHeap());
  //Serial.println(ESP.getSketchSize());
  //Serial.println(ESP.getFreeSketchSpace());

  delay(100);
}

