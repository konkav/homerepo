/*************************************************************
  Pin Label   Function                    Power/Input/Output       Notes
  VM          Motor Voltage               Power                    This is where you provide power for the motors (2.2V to 13.5V)
  VCC         Logic Voltage               Power                    This is the voltage to power the chip and talk to the microcontroller (2.7V to 5.5V)
  GND         Ground                      Power                    Common Ground for both motor voltage and logic voltage (all GND pins are connected)
  STBY        Standby                     Input                    Allows the H-bridges to work when high (has a pulldown resistor so it must actively pulled high)
  AIN1/BIN1   Input 1 for channels A/B    Input                    One of the two inputs that determines the direction.
  AIN2/BIN2   Input 2 for channels A/B    Input                    One of the two inputs that determines the direction.
  PWMA/PWMB   PWM input for channels A/B  Input                    PWM input that controls the speed
  A01/B01     Output 1 for channels A/B   Output                   One of the two outputs to connect the motor
  A02/B02     Output 2 for channels A/B   Output                   One of the two outputs to connect the motor


  In1  In2  PWM   Out1   Out2   Mode
  H    H    H/L   L      L      Short brake
  L    H    H     L      H      CCW
  L    H    L     L      L      Short brake
  H    L    H     H      L      CW
  H    L    L     L      L      Short brake
  L    L    H     OFF    OFF    Stop
**************************************************************/

const byte BL = 1; //LEFT
const byte BR = 2; //RIGHT
const byte BF = 3; //FORWARD
const byte BA = 4; //RANDOM
const byte BS1 = 5; //SUBROUTINE 1
const byte BS2 = 6; //SUBROUTINE 2

#include <ESP8266WiFi.h>
char APssid[20] = "CubettoClone";
char APpass[20] = "asdfghjk";
IPAddress controllerIP(10, 10, 10, 10);
IPAddress robotIP(10, 10, 10, 20);
IPAddress subnet(255, 255, 255, 0);
IPAddress gateway(10, 10, 10, 1);
byte port = 23;
// WiFiServer wifiserver(23);
WiFiClient controller;

const byte AO = 14; // AIN1
const byte AT = 12; // AIN2
const byte BO = 0; // BIN1
const byte BT = 4; // BIN2
const byte PWMA = 13;
const byte PWMB = 5;
const byte STBY = 2;
byte menu = 99;
char inpt = 0; // input from serial
byte seq; // sequence nr.
byte robocomm = 0; // semaphore for communication
byte msgend = 85; //msg terminator
byte togo = 0; // next thing to do
unsigned long movetimer; // for timing movements
byte ctrlid[4]; // controller ID received in init msg
byte robomsg[10] = {0}; // message cache, robot will send this
byte ctrlmsg[10]; // message cache, controllers response
// 0,1,2,3 - robot/ctrl ID , 4 - who? 2=ctrl 1=robot, 5 - unused, 6 - sequence nr., 7 - command, 8 -command data, 9 - msgend
// 7 (8) - command: 1 - init msg (1-robo->ctrl,2-ctrl->robo), 2 - sync sequence nr.(1-ctrl request,2-robo reply), 3 - self data (battery voltage, etc.), 4 - robot led, 5 - movement data
// robocomm: 0 - no connection, 1 - IP connected, comm init sending, 2 - comm init received, 3 - command requested, 4 - command arrived


void sendmsg() {
  Serial.println("Sending message:");
  for (byte x = 0; x < 10; x++) {
    Serial.print(x);
    Serial.print(". byte: ");
    Serial.println(robomsg[x]);
    controller.write(robomsg[x]);
  }
  Serial.println();
}




void setup() {
  Serial.begin ( 115200 );
  delay(100);
  Serial.println("Initializing...");
  Serial.println(WiFi.status());
  //Serial.println(ESP.getCoreVersion());
  //Serial.setDebugOutput(true);

  pinMode(AO, OUTPUT);
  pinMode(AT, OUTPUT);
  pinMode(BO, OUTPUT);
  pinMode(BT, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(STBY, OUTPUT);

  digitalWrite(STBY, LOW);

  controller.stop();
  WiFi.persistent(false);
  WiFi.disconnect();
  //WiFi.mode(WIFI_OFF);
  delay(100);
  //Serial.println(WiFi.status());
  WiFi.mode(WIFI_STA);
  //Serial.println(WiFi.status());
  WiFi.config(robotIP, gateway, subnet);
  //Serial.println(WiFi.status());
  WiFi.begin(APssid, APpass);
  //Serial.println(WiFi.status());

  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    //Serial.print(".");
    Serial.println(WiFi.status());
    //WiFi.printDiag(Serial);
  }

  Serial.println();
  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("subnet mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("gateway: ");
  Serial.println(WiFi.gatewayIP());

  randomSeed(micros());
  for (byte x = 0; x < 4; x++) {
    robomsg[x] = random(256);
  }
  robomsg[4] = 1;
  robomsg[9] = msgend;
  // comm test !!!@@@!!!
  robomsg[0] = 11;
  robomsg[1] = 22;
  robomsg[2] = 33;
  robomsg[3] = 44;

  // wifiserver.begin();
  // wifiserver.setNoDelay(true);

}

void loop() {


  if (!controller.connected()) {
    Serial.println("Connecting to controller");
    controller.connect(controllerIP, port);
    robocomm = 0;
    seq = 1;
  }
  if (controller.connected() && robocomm == 0) {
    Serial.println("Connected to controller, initializing the comm.");
    // init the comm
    robomsg[6] = seq;
    robomsg[7] = 1;
    robomsg[8] = 1;
    // This will send the request to the controller
    sendmsg();
    robocomm = 1;
  }

  if (controller.connected() && robocomm == 2) {
    if (ctrlmsg[4] == 2 && ctrlmsg[7] == 1 && ctrlmsg[8] == 2 ) {
      Serial.println("Controllers ID:");
      for (byte x = 0; x < 4; x++) {
        ctrlid[x] = ctrlmsg[x];
        Serial.print(ctrlid[x]);
      }
      Serial.println();
      robocomm = 3;
    } else {
      robocomm = 0; // wrong answer
      Serial.println("Wrong answer at robocomm 2. Resetting to robocomm 0");
    }
  }


  if (controller.connected() && robocomm == 4) {
    if (memcmp(ctrlmsg, ctrlid, 4) != 0) {
      Serial.println("Wrong ctrlid.");
    } else {
      if (ctrlmsg[4] == 2 && ctrlmsg[7] == 2 && ctrlmsg[8] == 1) { // seq sync request reply
        Serial.println("Sequence nr. sync request replying.");
        seq = 1;
        robomsg[6] = seq;
        robomsg[7] = 2;
        robomsg[8] = 2;
        sendmsg();
        robocomm = 3;
      }
      if (ctrlmsg[4] == 2 && ctrlmsg[7] == 5 ) {
        if (ctrlmsg[6] > seq) {
          Serial.print("Direction request received. [8] value: ");
          Serial.print(ctrlmsg[8]);
          Serial.println();
          seq = ctrlmsg[6];
          togo = ctrlmsg[8];
          robocomm = 6;
        }
      }
    }
  }

  if (controller.connected() && robocomm == 6) {
    if (togo >= 100 ) {
      Serial.print("Movement finished. togo value: ");
      Serial.print(togo);
      Serial.println();
      Serial.println("Sending ACK to controller.");
      robomsg[6] = seq;
      robomsg[7] = 5;
      robomsg[8] = (togo - 100);
      sendmsg();
      robocomm = 3;
    }
  }

  // memcmp(ctrlmsg, robomsg, 4);
  // 0,1,2,3 - robot/ctrl ID , 4 - who? 2=ctrl 1=robot, 5 - unused, 6 - sequence nr., 7 - command, 8 -command data, 9 - msgend
  // 7 (8) - command: 1 - init msg (1-robo->ctrl,2-ctrl->robo), 2 - sync sequence nr.(1-ctrl request,2-robo reply), 3 - self data (battery voltage, etc.), 4 - robot led, 5 - movement data
  // robocomm: 0 - no connection, 1 - IP connected, comm init sending, 2 - comm init received, 3 - command requested, 4 - command arrived

  //for (byte x = 0; x < 3; x++) {
  //delay(1000);
  if (controller.available() > 9 && (robocomm == 1 || robocomm == 3)) {
    Serial.println("Msg from controller.");
    for (byte y = 0; y < 10; y++) {
      ctrlmsg[y] = controller.read();
      Serial.print(ctrlmsg[y]);
    }
    Serial.println();
    if (ctrlmsg[9] == msgend) {
      Serial.println("Msg ended OK.");
      robocomm++;
    }
    //while (controller.available()) controller.read(); // empty IP buffer
    //Serial.write(ctrlmsg[0]);
  }
  //}

  if (togo > 0 && togo < 100) {
    if (movetimer == 0) {
      movetimer = millis() + 2050; // 2s is the time value for a move
      Serial.print("movetimer = ");
      Serial.println(movetimer);
      switch (togo) {
        case BL:
          Serial.println("Going left.");
          digitalWrite(AO, LOW);
          digitalWrite(AT, HIGH);
          digitalWrite(BO, LOW);
          digitalWrite(BT, HIGH);
          analogWrite(PWMA, 400);
          analogWrite(PWMB, 400);
          digitalWrite(STBY, HIGH);
          break;
        case BR:
          Serial.println("Going right.");
          digitalWrite(AO, HIGH);
          digitalWrite(AT, LOW);
          digitalWrite(BO, HIGH);
          digitalWrite(BT, LOW);
          analogWrite(PWMA, 400);
          analogWrite(PWMB, 400);
          digitalWrite(STBY, HIGH);
          break;
        case BF:
          Serial.println("Going forward.");
          digitalWrite(AO, HIGH);
          digitalWrite(AT, LOW);
          digitalWrite(BO, LOW);
          digitalWrite(BT, HIGH);
          analogWrite(PWMA, 400);
          analogWrite(PWMB, 400);
          digitalWrite(STBY, HIGH);
          break;
        default:
          Serial.print("Unimplemented command, togo value:");
          Serial.print(togo);
          Serial.println();
          movetimer = 0;
          togo += 100;
          digitalWrite(STBY, LOW);
          break;
      }
    } else {
      if (movetimer <= millis()) {
        digitalWrite(STBY, LOW);
        Serial.print("Stopping. Exact time: ");
        Serial.println(millis());
        movetimer = 0;
        togo += 100;
      }
    }
  }

  delay(100);
}

