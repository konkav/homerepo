// ESP8266, DIO

#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#define MAX_SRV_CLIENTS 1

#include "sensitive.h"
// some sensitive informations, like wifi ssid and psk

WiFiServer server(port);
WiFiClient serverClients[MAX_SRV_CLIENTS];

#include <Wire.h>
#include <Adafruit_MPR121.h>
Adafruit_MPR121 touchModule = Adafruit_MPR121();
const byte touchAddress = 0x5A;
// address pin tied to: GND - 0x5A; VCC - 0x5B; SDA - 0x5C; SCL - 0x5D
unsigned int touchInput;
unsigned int lastTouch;


#include <NeoPixelBus.h>
const uint16_t PixelCount = 7; // 7 LEDs
// output pin is GPIO3, DMA pin for ESP8266
#define colorSaturation 32
NeoPixelBus<NeoGrbwFeature, NeoEsp8266Dma800KbpsMethod> strip(PixelCount);

RgbwColor clrs[] = {
  RgbwColor (0),
  RgbwColor (colorSaturation, 0, 0, 0),
  RgbwColor (0, colorSaturation, 0, 0),
  RgbwColor (0, 0, colorSaturation, 0),
  RgbwColor (0, 0, 0, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, 0, 0),
  RgbwColor (colorSaturation, 0, colorSaturation, 0),
  RgbwColor (colorSaturation, 0, 0, colorSaturation),
  RgbwColor (0, colorSaturation, colorSaturation, 0),
  RgbwColor (0, colorSaturation, 0, colorSaturation),
  RgbwColor (0, 0, colorSaturation, colorSaturation),
  RgbwColor (0, colorSaturation, colorSaturation, colorSaturation),
  RgbwColor (colorSaturation, 0, colorSaturation, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, 0, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, colorSaturation, 0),
  RgbwColor (colorSaturation, colorSaturation, colorSaturation, colorSaturation),
};


const int LED = 2;

byte cc = 0;
byte t = 0;
byte d = 0;

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  delay(500); // give me time to bring up serial monitor
  Serial.println("\n\nStarting...V1");
  Serial.print("ESP8266 RGBW Touch OTA IP: ");

  WiFi.hostname("bedheadlamp");
  WiFi.mode(WIFI_STA);
  WiFi.config(IP, gateway, subnet);
  WiFi.begin(ssid, pass);
  MDNS.begin("bedheadlamp");
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED, !digitalRead(LED));
    delay(500);
  }
  digitalWrite(LED, HIGH);
  server.begin();
  server.setNoDelay(true);
  Serial.println(WiFi.localIP());

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    //Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    //Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR); // Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR); // Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR); // Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR); // Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR); // Serial.println("End Failed");
  });
  ArduinoOTA.begin();

  Serial.println("Strip start");
  strip.Begin();
  strip.Show();

  Serial.println("Wire.h start");
  Wire.begin(4, 5); // (SDA,SCL) - GPIO4=D2, GPIO5=D1
  Serial.println("Touch begin");
  touchModule.begin(touchAddress);
}

void loop() {
  ArduinoOTA.handle();

  touchInput = touchModule.touched();
  if (touchInput != lastTouch) {
    Serial.println("touched");
    // do the magic
    // serverClients[0].print("Touch happened: ");
    // serverClients[0].println(touchInput);
    lastTouch = touchInput;
  }

  if (server.hasClient()) {
    byte i;
    digitalWrite(LED, 0);
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if (serverClients[i]) {
          serverClients[i].stop();
        }
        serverClients[i] = server.available();
        serverClients[i].println("Hi. Off, White, Red, Green, Blue, Something");
        break;
      }
    }
    //no free/disconnected spot so reject
    if (i == MAX_SRV_CLIENTS) {
      WiFiClient serverClient = server.available();
      serverClient.stop();
      // Serial1.println("Connection rejected ");
    }
  }

  for (byte i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i] && serverClients[i].connected()) {
      if (serverClients[i].available()) {
        //get data from the telnet client
        digitalWrite(LED, 0);
        char inpt;
        char inpt2[5];
        byte x = 0;
        inpt = serverClients[i].read();
        while (serverClients[i].available()) {
          inpt2[x] = serverClients[i].read();
          if (x < 4) x++;
        }
        digitalWrite(LED, 1);
        if (inpt == 'w') {
          cc = 4;
          serverClients[i].print("White: ");
          serverClients[i].println(cc);
        }
        if (inpt == 'r') {
          cc = 1;
          serverClients[i].print("Red: ");
          serverClients[i].println(cc);
        }
        if (inpt == 'g') {
          cc = 2;
          serverClients[i].print("Green: ");
          serverClients[i].println(cc);
        }
        if (inpt == 'b') {
          cc = 3;
          serverClients[i].print("Blue: ");
          serverClients[i].println(cc);
        }
        if (inpt == 'o') {
          cc = 0;
          serverClients[i].print("OFF: ");
          serverClients[i].println(cc);
        }
        if (inpt == '+') {
          cc++;
          if (cc > 15) cc = 0;
          serverClients[i].print("+: ");
          serverClients[i].println(cc);
        }
        if (inpt == '-') {
          if (cc < 1) cc = 16;
          cc--;
          serverClients[i].print("-: ");
          serverClients[i].println(cc);
        }
        serverClients[i].print("Touch happened: ");
        serverClients[i].println(touchInput);
      }
    }
  }

  if (d != cc) {
    for (byte x = 0; x < PixelCount; x++) {
      strip.SetPixelColor(x, clrs[cc]);
    }
    strip.Show();
    d = cc;
    //Serial.println(cc);
  }

}
