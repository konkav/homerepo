This is a sketch for ESP8266 on a car with two DC motors and a TB6612FNG. It will listen on a TCP port for three bytes, first is for enabling/disabling the motor driver, then two signed 8 bit ints (2D joystick type, X and Y, where (0,0) is the center), and it will drive the motors. Y value represents a difference in the motors PWM, where Y=0 -> PWMA=PWMB (turning in place).

It need a sensitive.h file with the content below, or just include it in the sketch in place of the file include:

const char ssid[] = "";
const char pass[] = "";
IPAddress IP(0, 0, 0, 0);
IPAddress subnet(0, 0, 0, 0);
IPAddress gateway(0, 0, 0, 0);
byte port = 0;

OTA update included.