// ESP8266, DIO

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>

#include "sensitive.h"
// some sensitive informations, like wifi ssid and psk, ip addresses, etc. look at the Readme.txt

//              GPIO        NodeMCU
const byte AO = 14;      // D5            AIN1
const byte AT = 12;      // D6            AIN2
const byte BO = 0;       // D3            BIN1
const byte BT = 4;       // D2            BIN2
const byte PWMA = 13;    // D7            1=L 2=H -> CCW
const byte PWMB = 5;     // D1            1=H 2=L -> CW
const byte STBY = 2;     // D4

byte stbypin;
sint8_t x = 0;  // enough for crude PWM
sint8_t y = 0;

int a = 0;
int b = 0;

byte newdata = 0; // semaphore for new incoming data

#define MAX_SRV_CLIENTS 1
WiFiServer server(port);
WiFiClient serverClients[MAX_SRV_CLIENTS];

void setup() {
  analogWriteRange(255);  // default 1023
  analogWriteFreq(1000);  // default 1000 Hz
  pinMode(AO, OUTPUT);
  pinMode(AT, OUTPUT);
  pinMode(BO, OUTPUT);
  pinMode(BT, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(STBY, OUTPUT);
  digitalWrite(STBY, 0);

  Serial.begin(115200);
  delay(100);
  Serial.println("\n\nStarting...V1\n\n");

  WiFi.hostname("arducar");
  WiFi.mode(WIFI_STA);
  WiFi.config(IP, gateway, subnet);
  WiFi.begin(ssid, pass);
  MDNS.begin("arducar");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  server.begin();
  server.setNoDelay(true);

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }
    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
}



void loop() {
  ArduinoOTA.handle();
  uint8_t i;

  //check if there are any new clients
  if (server.hasClient()) {
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if (serverClients[i]) {
          serverClients[i].stop();
        }
        serverClients[i] = server.available();
        Serial.print("New client: "); Serial.println(i);
        while (serverClients[i].available()) serverClients[i].read();
        break;
      }
    }
    //no free/disconnected spot so reject
    if (i == MAX_SRV_CLIENTS) {
      WiFiClient serverClient = server.available();
      serverClient.stop();
      Serial.println("Connection rejected ");
    }
  }

  //check clients for data
  for (i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i] && serverClients[i].connected()) {
      if (serverClients[i].available()) {
        //get data from TCP port, 3 bytes, first is a kill-switch, last two are signed bytes
        if (serverClients[i].available() >= 3) {
          Serial.print("Serial data available: ");
          newdata = 1;
          Serial.println(serverClients[i].available());
          stbypin = serverClients[i].read(); // - 48;
          Serial.print("stbypin: ");
          Serial.println(stbypin);
          x = serverClients[i].read(); // - 48;
          Serial.print("x: ");
          Serial.println(x);
          y = serverClients[i].read(); // - 48;
          Serial.print("y: ");
          Serial.println(y);
          while (serverClients[i].available()) serverClients[i].read();
          /*
           if (x < 6) {
            x *= 20;
          } else {
            x = (5 - x) * 20;
          }

          if (y < 6) {
            y *= 20;
          } else {
            y = (5 - y) * 20;
          }
          */
        }
      }
    }
  }

  if (stbypin == 1) {
    digitalWrite(STBY, 1);
  }  else {
    digitalWrite(STBY, 0);
  }

  a = x + y;
  b = y - x;

  if (a < 0) {
    digitalWrite(AO, 0);      // A motor CCW
    digitalWrite(AT, 1);
    a = abs(a);
  } else {
    digitalWrite(AO, 1);      // A motor CW
    digitalWrite(AT, 0);
  }

  if (a != 0) a = a + 50;

  if (b < 0) {
    digitalWrite(BO, 1);      // B motor CW
    digitalWrite(BT, 0);
    b = abs(b);
  } else {
    digitalWrite(BO, 0);      // B motor CCW
    digitalWrite(BT, 1);
  }

  if (b != 0) b = b + 50;

  if (a > 255) a = 255;
  if (b > 255) b = 255;

  analogWrite(PWMA, a);
  analogWrite(PWMB, b);

  Serial.print("stbypin: "); Serial.println(stbypin);
  Serial.print("X: "); Serial.println(x);
  Serial.print("Y: "); Serial.println(y);
  Serial.print("A: "); Serial.println(a);
  Serial.print("B: "); Serial.println(b);
  Serial.println("---------------------------------------");
  if (serverClients[0] && serverClients[0].connected() && newdata == 1) {
    serverClients[0].print("stbypin: "); serverClients[0].println(stbypin);
    serverClients[0].print("X: "); serverClients[0].println(x);
    serverClients[0].print("Y: "); serverClients[0].println(y);
    serverClients[0].print("A: "); serverClients[0].println(a);
    serverClients[0].print("B: "); serverClients[0].println(b);
    serverClients[0].println("---------------------------------------");
    newdata = 0;
  }
  delay(1000);
  analogWrite(PWMA, 0);
  analogWrite(PWMB, 0);
  delay(2000);
  x = 0;
  y = 0;
}
