// MCP23017
#include <Wire.h>
#define MCP 0x20 // B0100+A2+A1+A0 => B0100000
#define GPIOA 0x12 // PORTA
#define IODIRA 0x00 // I/O DIRECTION REGISTER A
#define IOCON 0x0A // CONFIGURATION REGISTER
byte i2cSDA = 12; // D6
byte i2cSCL = 14; // D5
unsigned int i2cSPEED = 400000;

byte motordata = 0; // output for the two ULN's
byte motordata_writen = 0; // output for the two ULN's writen to the MCP
byte m1_steps[8] = {B00000001, B00000011, B00000010, B00000110, B00000100, B00001100, B00001000, B00001001}; // motor 1 steps, lower bits
byte m2_steps[8] = {B00010000, B00110000, B00100000, B01100000, B01000000, B11000000, B10000000, B10010000}; // motor 2 steps, higher bits
byte m1_ap; // the output posision in the steps array
byte m2_ap;

unsigned int go_steps = 4500;
unsigned int turn_steps = 3637;
unsigned int step_time = 1400;
unsigned long ptime; // micros() value of the previous step
unsigned long atm; // micros() value in the begining of the loop()
int m1_step_counter; // for counting the remaining steps, signed, positive = CW, negative = CCW
int m2_step_counter;

//char inpt; // input
//char inpt2[5]; // input

#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#define MAX_SRV_CLIENTS 1
#include "sensitive.h"
// some sensitive informations, like wifi ssid and psk, ip addresses, etc.
const int LED = 2;

WiFiServer server(port);
WiFiClient serverClients[MAX_SRV_CLIENTS];

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  delay(100);
  Serial.println("\n\nStarting...V5");

  WiFi.hostname("robo01");
  WiFi.mode(WIFI_STA);
  WiFi.config(IP, gateway, subnet);
  WiFi.begin(ssid, pass);
  MDNS.begin("robo01");
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED, !digitalRead(LED));
    delay(500);
  }
  digitalWrite(LED, 1);
  server.begin();
  server.setNoDelay(true);

  Wire.setClock(i2cSPEED);
  Wire.begin(i2cSDA, i2cSCL);

  //Wire.beginTransmission(MCP); // no address increments
  //Wire.write(IOCON); // pointer
  //Wire.write(B00100000); // SEQOP disable
  //Wire.endTransmission();

  Wire.beginTransmission(MCP); // setup out direction registers
  Wire.write(IODIRA); // pointer
  Wire.write(0x00); // PortA all output
  Wire.endTransmission();

  Wire.beginTransmission(MCP); // set output pins to 0
  Wire.write(GPIOA);
  Wire.write(B00000000);
  Wire.endTransmission();

  //  ptime = micros();
  //  Wire.beginTransmission(MCP);
  //  Wire.write(GPIOA);
  //  Wire.write(B10101010);
  //  Serial.println(Wire.endTransmission());
  //  atm = micros();
  //  Serial.println(atm - ptime);

  delay(1000);

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("OTA ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  delay(1000);
  Serial.println("\nSend new commands over telnet.");

}

void loop() {
  ArduinoOTA.handle();
  atm = micros() - ptime;

  if (atm >= step_time) {
    ptime = micros();
    if (m1_step_counter > 0 ) { // M1 step CW
      if (m1_ap == 0) m1_ap = 8;
      m1_ap--;
      m1_step_counter--;
      //if (m1_step_counter == 0) Serial.println("\nMotor 1 CW: All steps steped, stepping stopped.");
    }
    if (m1_step_counter < 0 ) { // M1 step CCW
      m1_ap++;
      if (m1_ap == 8) m1_ap = 0;
      m1_step_counter++;
      //if (m1_step_counter == 0) Serial.println("\nMotor 1 CCW: All steps steped, stepping stopped.");
    }
    if (m2_step_counter > 0 ) { // M2 step CW
      if (m2_ap == 0) m2_ap = 8;
      m2_ap--;
      m2_step_counter--;
      //if (m2_step_counter == 0) Serial.println("\nMotor 2 CW: All steps steped, stepping stopped.");
    }
    if (m2_step_counter < 0 ) { // M2 step CCW
      m2_ap++;
      if (m2_ap == 8) m2_ap = 0;
      m2_step_counter++;
      //if (m2_step_counter == 0) Serial.println("\nMotor 2 CCW: All steps steped, stepping stopped.");
    }
    if (m1_step_counter == 0 && m2_step_counter == 0) {
      motordata = 0;
    } else {
      motordata = m1_steps[m1_ap] + m2_steps[m2_ap];
    }
  }

  if (motordata_writen != motordata) {
    Wire.beginTransmission(MCP); // write calculated motordata to MCP
    Wire.write(GPIOA);
    Wire.write(motordata);
    Wire.endTransmission();
    motordata_writen = motordata;
    //Serial.print("Motordata: ");
    //Serial.println(motordata, BIN);
  }


  /*
    if (Serial.available()) {
    inpt = Serial.read();
    while (Serial.available()) Serial.read(); // empty serial buffer
    Serial.print("\nCommand readed:");
    Serial.println(inpt);
    if (inpt == 'f') {
      Serial.println("Going forward, steps:");
      Serial.println(steps);
      m1_step_counter = steps;
      m2_step_counter = 0 - steps;
    }
    if (inpt == 'b') {
      Serial.println("Going backward, steps:");
      Serial.println(steps);
      m1_step_counter = 0 - steps;
      m2_step_counter = steps;
    }
    if (inpt == 'r') {
      Serial.println("Going right, steps:");
      Serial.println(steps / 2);
      m1_step_counter = steps;
      m2_step_counter = steps;
    }
    if (inpt == 'l') {
      Serial.println("Going left, steps:");
      Serial.println(steps / 2);
      m1_step_counter = 0 - steps;
      m2_step_counter = 0 - steps;
    }
    }
  */

  if (server.hasClient()) {
    byte i;
    digitalWrite(LED, 0);
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if (serverClients[i]) {
          serverClients[i].stop();
        }
        serverClients[i] = server.available();
        serverClients[i].println("Hi. s - step_time, g - go_steps, t - turn_steps, f, b, l, r");
        // Serial1.print("New client: "); Serial1.print(i);
        break;
      }
    }
    //no free/disconnected spot so reject
    if (i == MAX_SRV_CLIENTS) {
      WiFiClient serverClient = server.available();
      serverClient.stop();
      // Serial1.println("Connection rejected ");
    }
  }

  for (byte i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i] && serverClients[i].connected()) {
      if (serverClients[i].available()) {
        //get data from the telnet client
        digitalWrite(LED, 0);
        char inpt;
        char inpt2[5];
        byte x = 0;
        inpt = serverClients[i].read();
        while (serverClients[i].available()) {
          inpt2[x] = serverClients[i].read();
          if (x < 4) x++;
        }
        digitalWrite(LED, 1);
        if (inpt == 's') {
          step_time = (inpt2[0] - 48) * 1000 + (inpt2[1] - 48) * 100 + (inpt2[2] - 48) * 10 + (inpt2[3] - 48);
          serverClients[i].print("Changed step_time: ");
          serverClients[i].println(step_time);
        }
        if (inpt == 'g') {
          go_steps = (inpt2[0] - 48) * 1000 + (inpt2[1] - 48) * 100 + (inpt2[2] - 48) * 10 + (inpt2[3] - 48);
          serverClients[i].print("Changed go_steps: ");
          serverClients[i].println(go_steps);
        }
        if (inpt == 't') {
          turn_steps = (inpt2[0] - 48) * 1000 + (inpt2[1] - 48) * 100 + (inpt2[2] - 48) * 10 + (inpt2[3] - 48);
          serverClients[i].print("Changed turn_steps: ");
          serverClients[i].println(turn_steps);
        }
        if (inpt == 'f') {
          serverClients[i].println("Going forward, steps:");
          serverClients[i].println(go_steps);
          m1_step_counter = go_steps;
          m2_step_counter = 0 - go_steps;
        }
        if (inpt == 'b') {
          serverClients[i].println("Going backward, steps:");
          serverClients[i].println(go_steps);
          m1_step_counter = 0 - go_steps;
          m2_step_counter = go_steps;
        }
        if (inpt == 'r') {
          serverClients[i].println("Going right, steps:");
          serverClients[i].println(turn_steps);
          m1_step_counter = turn_steps;
          m2_step_counter = turn_steps;
        }
        if (inpt == 'l') {
          serverClients[i].println("Going left, steps:");
          serverClients[i].println(turn_steps);
          m1_step_counter = 0 - turn_steps;
          m2_step_counter = 0 - turn_steps;
        }
      }
    }
  }
}
