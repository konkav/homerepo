This is a sketch for ESP8266. It drives 28BYJ48 steppers over MCP23017 I2C port expander. ULN2003 inputs are connected directly to MCP outputs (2 x 4 pin). It can be controlled over serial (USB) or over telnet. 
It need a sensitive.h file with the content below, or just include it in the sketch in place of the file include:

const char ssid[] = "";
const char pass[] = "";
IPAddress IP(0, 0, 0, 0);
IPAddress subnet(0, 0, 0, 0);
IPAddress gateway(0, 0, 0, 0);
byte port = 0;

OTA update included.