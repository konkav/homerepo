NEVER TESTED.

This is a sketch for ESP32. It drives 7 of smart RGBW leds, and one pin touch sensing.

It needs a sensitive.h file, but at the moment, not using data within it.

OTA update NOT included, but it will be.

Goals:
1. if wifi not disabled, first try to connect to the saved network. if unable for 20 seconds, switch to AP mode.
2. web GUI.
3. BLE. could be used to signal data from mobile, like: blink blue if there is an unread message, red for unanswered calls, green for unread emails, etc.
4. in battery mode, should somehow display if the voltage is low.
5. effects saved in spiffs.