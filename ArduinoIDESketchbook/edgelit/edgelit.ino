#include <NeoPixelBus.h>

#include "sensitive.h"
// some sensitive informations, like wifi ssid and psk

const uint16_t PixelCount = 7; // this example assumes 7 pixels, making it smaller will cause a failure
const uint8_t PixelPin = 13;  // make sure to set this to the correct pin, ignored for Esp8266
#define colorSaturation 32
NeoPixelBus<NeoGrbwFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);

RgbwColor clrs[] = {
  RgbwColor (0),
  RgbwColor (colorSaturation, 0, 0, 0),
  RgbwColor (0, colorSaturation, 0, 0),
  RgbwColor (0, 0, colorSaturation, 0),
  RgbwColor (0, 0, 0, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, 0, 0),
  RgbwColor (colorSaturation, 0, colorSaturation, 0),
  RgbwColor (colorSaturation, 0, 0, colorSaturation),
  RgbwColor (0, colorSaturation, colorSaturation, 0),
  RgbwColor (0, colorSaturation, 0, colorSaturation),
  RgbwColor (0, 0, colorSaturation, colorSaturation),
  RgbwColor (0, colorSaturation, colorSaturation, colorSaturation),
  RgbwColor (colorSaturation, 0, colorSaturation, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, 0, colorSaturation),
  RgbwColor (colorSaturation, colorSaturation, colorSaturation, 0),
  RgbwColor (colorSaturation, colorSaturation, colorSaturation, colorSaturation),
};

#define TOUTCH_PIN T0 // ESP32 Pin D4
#define LED_PIN 2
int touch_value = 100;

byte cc = 0;
byte t = 0;
byte d = 0;

void setup() {
  Serial.begin(115200);
  touch_value = touchRead(TOUTCH_PIN);
  delay(1000); // give me time to bring up serial monitor
  Serial.println("ESP32 Touch+RGBW");
  pinMode(LED_PIN, OUTPUT);
  digitalWrite (LED_PIN, LOW);
  strip.Begin();
  strip.Show();
}

void loop() {
  touch_value = touchRead(TOUTCH_PIN);
  // Serial.println(touch_value);  // get value using T0
  if (touch_value < 45)  {  // not touched
    digitalWrite (LED_PIN, HIGH);
    if (t > 0) t--;
  }
  else  {  //touched
    digitalWrite (LED_PIN, LOW);
    if (t == 0) cc++;
    t = 20;
  }

  if (d != cc) {
    if (cc > 15) cc = 0;
    for (byte x = 0; x < PixelCount; x++) {
      strip.SetPixelColor(x, clrs[cc]);
    }
    strip.Show();
    d = cc;
    Serial.println(cc);
  }

}
