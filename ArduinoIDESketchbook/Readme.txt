To configure arduino IDE to compile code within this folder:


Set arduino IDE sketchbook folder to this root directory, i.e. >>FILE>>Preferences, 
on the Settings tab modify the "Sketchbook location" parameter to this directory.

This forces the compiler to first use libraries found in the Libraries folder of this sketchbook folder, 
if in future you need any more libraries unpack them in the Libraries folder so they can be versioned
with this projects source code. 
