/*************************************************************
  You can receive x and y coords for joystick movement within App.

  App project setup:
    Two Axis Joystick on V1 in MERGE output mode.
    MERGE mode means device will receive both x and y within 1 message
**************************************************************/

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

char auth[] = "???";
char ssid[] = "???";
char pass[] = "???";

const byte AO = 14;
const byte AT = 12;
const byte BO = 0;
const byte BT = 4;
const byte PWMA = 13;
const byte PWMB = 5;
const byte STBY = 2;

int x = 0;
int y = 0;
int a = 0;
int b = 0;


BLYNK_WRITE(V1) {
  int x = param[0].asInt();
  int y = param[1].asInt();
  
  a = x + y;
  b = y - x;
  
  if (a < 0) {
    digitalWrite(AO,0);
    digitalWrite(AT,1);
    a = abs(a);
  } else {
    digitalWrite(AO,1);
    digitalWrite(AT,0);
  }

  if (a != 0) a = a + 200;

  if (b < 0) {
    digitalWrite(BO,1);
    digitalWrite(BT,0);
    b = abs(b);
  } else {
    digitalWrite(BO,0);
    digitalWrite(BT,1);
  }

  if (b != 0) b = b + 200;

  if (a > 1023) a = 1023;
  if (b > 1023) b = 1023;
  
  analogWrite(PWMA, a);
  analogWrite(PWMB, b);
}

BLYNK_WRITE(V2) {
  int stbypin = param.asInt();
  if (stbypin == 1){
    digitalWrite(STBY,1);
  }
  else {
    digitalWrite(STBY,0);
  }    
}


void setup()
{
    Blynk.begin(auth, ssid, pass, IPAddress(192, 168, 100, 248));
    
    pinMode(AO, OUTPUT);
    pinMode(AT, OUTPUT);
    pinMode(BO, OUTPUT);
    pinMode(BT, OUTPUT);
    pinMode(PWMA, OUTPUT);
    pinMode(PWMB, OUTPUT);
    pinMode(STBY, OUTPUT);
}

void loop()
{
  Blynk.run();  
}

